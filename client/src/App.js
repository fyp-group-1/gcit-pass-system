import React, { useState } from "react";
import { BrowserRouter as Router, Route, Routes, Navigate, Outlet } from 'react-router-dom';
import { useEffect } from "react";
import Gkdashboard from "./components/gatekeeper/Gkdashboard";
import Login from './components/Login';
import HHRDashboard from "./components/HeadHumanResource/HHRDashboard";
import Ssodashboard from "./components/studentserviceofficer/Ssodashboard";
import PublicLandingPage from "./components/public/PublicLandingPage";
import AdminDashboard from "./components/admin/AdminDashboard";
import SCDashboard from "./components/studentcounsellor/SCDashboard";
import VisitorType from "./components/public/VisitorType";
import VisitPeriod from "./components/public/VisitPeriod";

function App() {
  const[isAuthenticated, setIsAuthenticated] = useState(!!localStorage.getItem("access_token"))
  const[userId, setUserId] = useState(localStorage.getItem('user_id'));
  const[userRole, setUserRole] = useState(localStorage.getItem('role'))
  const[visitorType, setVisitorType] = useState(localStorage.getItem('visitor_type'))
  const[visitPeriod, setVisitPeriod] = useState(localStorage.getItem('visit_period'))

  return (
    <div className="App"> 

      <Router>
        <Routes>
          <Route path="/">
                    <Route index element={isAuthenticated ?   <Navigate to={`/${userRole.replace(/"/g, '').replace(/\s/g, '').toLowerCase()}`} /> : <Login setIsAuthenticated={setIsAuthenticated} setUserId={setUserId} setUserRole={setUserRole} />}></Route>
                    <Route path='public' element={<PublicLandingPage setVisitorType={setVisitorType} setVisitPeriod={setVisitPeriod}></PublicLandingPage>}> </Route>
                    <Route path='visitortype' element={<VisitorType setVisitorType={setVisitorType} setVisitPeriod={setVisitPeriod}></VisitorType>}> </Route>
                    <Route path='visitperiod' element={<VisitPeriod setVisitorType={setVisitorType} setVisitPeriod={setVisitPeriod}></VisitPeriod>}> </Route>
          </Route>

          {
            userRole === "\"Gatekeeper\"" &&
            <Route path="/gatekeeper/*" element={isAuthenticated ? (<Outlet />) : (<Navigate to="/" />)}>
              <Route index element={<Gkdashboard setIsAuthenticated={setIsAuthenticated} />} />
              <Route path="page1" element={<PublicLandingPage />} />
            </Route>
          } 

          {
            userRole === "\"Student Service Officer\"" &&
            <Route path="/studentserviceofficer/*" element={isAuthenticated ? (<Outlet />) : (<Navigate to="/" />)}>
              <Route index element={<Gkdashboard setIsAuthenticated={setIsAuthenticated} />} />
              <Route path="page1" element={<PublicLandingPage />} />
            </Route>
          }

          {
            userRole === "\"Admin\"" &&
            <Route path="/admin/*" element={isAuthenticated ? (<Outlet />) : (<Navigate to="/" />)}>
              <Route index element={<Gkdashboard setIsAuthenticated={setIsAuthenticated} />} />
              <Route path="page1" element={<PublicLandingPage />} />
            </Route>
          }

          {
            userRole === "\"Student Counsellor\"" &&
            <Route path="/studentcounsellor/*" element={isAuthenticated ? (<Outlet />) : (<Navigate to="/" />)}>
              <Route index element={<SCDashboard setIsAuthenticated={setIsAuthenticated} />} />
            
              <Route path="page1" element={<PublicLandingPage />} />
            </Route>
          }
          {
            userRole === "\"Human Resource Head\"" &&
            <Route path="/humanresourcehead/*" element={isAuthenticated ? (<Outlet />) : (<Navigate to="/" />)}>
              <Route index element={<HHRDashboard setIsAuthenticated={setIsAuthenticated} />} />
              <Route path="page1" element={<PublicLandingPage />} />
            </Route>
          }
          {
            userRole === "\"Human Resource\"" &&
            <Route path="/humanresource/*" element={isAuthenticated ? (<Outlet />) : (<Navigate to="/" />)}>
              <Route index element={<HHRDashboard setIsAuthenticated={setIsAuthenticated} />} />
              <Route path="page1" element={<PublicLandingPage />} />
            </Route>
          }

         

        </Routes>
    </Router>
   
    </div>
  );
}

export default App;
