import React from 'react'
import Sidebar from './sidebar/Sidebar';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './navbar.css'
function SCStudentlog({ setIsAuthenticated }) {
  // const handleLogout = () => {
  //   // Clear the access token from local storage
  //   localStorage.removeItem('access_token');
  //   setIsAuthenticated(localStorage.removeItem('access_token'))
  // };

  return (
    <div class="row vh-100 vw-100">
      {console.log(localStorage.getItem('access_token'))}
      <div class="col-3">
        <Sidebar setIsAuthenticated={setIsAuthenticated} ></Sidebar>
      </div>
      {/* main container */}
      <div class="col-9 container d-flex flex-column ">
        <nav class="navbar navbar-expand-lg navbar-light bg-white p-4 row">
          <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          <div class="collapse navbar-collapse bg-" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 pe-5">
              <li class="nav-item">

                <button type="button" class="btn btn-custom btn-lg mx-auto" hidden>Large button</button>
              </li>
            </ul>
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
           
          </ul>
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link m-0 p-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <div className="row p-0 m-0 d-flex flex-nowrap justify-content-center align-items-center w-100 ">
                  <div className="col-4 m-0 p-0 d-flex justify-content-center align-items-center" style={{width: "45px"}}>
                      <svg
              width={38}
              height={38}
              viewBox="0 0 36 36"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <circle cx="17.5292" cy="17.5292" r="17.5292" fill="#FFC145" />
              <mask
                id="mask0_222_7594"
                style={{ maskType: "alpha" }}
                maskUnits="userSpaceOnUse"
                x={0}
                y={0}
                width={36}
                height={36}
              >
                <circle cx="17.7011" cy="17.5292" r="17.5292" fill="#FFC145" />
              </mask>
              <g mask="url(#mask0_222_7594)">
                <mask
                  id="mask1_222_7594"
                  style={{ maskType: "alpha" }}
                  maskUnits="userSpaceOnUse"
                  x={0}
                  y={0}
                  width={36}
                  height={36}
                >
                  <circle cx="17.7011" cy="17.5292" r="17.5292" fill="#FFC145" />
                </mask>
                <g mask="url(#mask1_222_7594)">
                  <path
                    d="M28.5328 37.1468V33.566C28.5328 31.6666 27.9622 29.845 26.9464 28.5019C25.9306 27.1588 24.5529 26.4043 23.1164 26.4043H12.2836C10.8471 26.4043 9.46939 27.1588 8.45362 28.5019C7.43784 29.845 6.86719 31.6666 6.86719 33.566V37.1468"
                    fill="#4F4F4F"
                  />
                  <path
                    d="M17.6996 20.9871C20.691 20.9871 23.116 18.5621 23.116 15.5707C23.116 12.5793 20.691 10.1543 17.6996 10.1543C14.7082 10.1543 12.2832 12.5793 12.2832 15.5707C12.2832 18.5621 14.7082 20.9871 17.6996 20.9871Z"
                    fill="white"
                  />
                </g>
              </g>
                      </svg>
                  </div>
                  <div className="col-8 m-0 p-0 ms-1 h-100 flex-grow-1 d-flex flex-column justify-content-center align-items-start" style={{height: '50px'}}>
                      <div className=' m-0 p-0 dm-600 text-nowrap text-dark' style={{fontSize: "14px"}}>Kezang Norbu</div>
                      <div className=' m-0 p-0 dm-500 '  style={{fontSize: "12px", color: '#AEAEAE'}}>Counciler</div>
                  </div>
                </div>
              </a>
              <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#">Change Password</a></li>
                
              </ul>
            </li>
          </ul>
        </div>
          </div>
        </nav> 
        {/* MAIN CONTAINER */}
        <div className="container scrolable-div w-100  py-5" style={{overflow: "auto", height: "425px"}}>
        <button className="btn btn-primary mb-2">Search</button>
          <button className="btn btn-secondary mb-2">Print</button>
          <button className="btn btn-secondary mb-2">Date</button>
        <table class="table table-striped table-xl bg-white centered-table">
              <thead>
                <tr>
                  <th scope="col" className='dm-600'>S/N</th>
                  <th scope="col" className='dm-600'>Student ID</th>
                  <th scope="col" className='dm-600'>Name</th>
                  <th scope="col" className='dm-600'>Gender</th>
                  <th scope="col" className='dm-600'>Programme</th>
                  <th scope="col" className='dm-600'>Semester</th>
                  <th scope="col" className='dm-600'>Check-out</th>
                  <th scope="col" className='dm-600'>Check-in</th>
                </tr>
              </thead>
              <tbody>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted'>
                <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted'>
                <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted'>
                <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kezang Norbu</td>
                  <td>Male</td>
                  <td>computer science</td>
                  <td>7</td>
                  <td>Done</td>
                  <td>-</td>
                </tr>
              </tbody>
            </table>
        </div>
      </div>
    </div>
    
  )
}

export default SCStudentlog;