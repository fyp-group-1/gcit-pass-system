import React from 'react';
import Sidebar from './sidebar/Sidebar';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './navbar.css';

function CreateRole({ setIsAuthenticated }) {
  return (
    <div className="row vh-100 vw-100">
      <div className="col-3">
        <Sidebar setIsAuthenticated={setIsAuthenticated}></Sidebar>
      </div>
      <div className="col-9 container d-flex flex-column">
        <nav className="navbar navbar-expand-lg navbar-light bg-white p-4 row">
          <div className="container-fluid">
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse bg-" id="navbarSupportedContent">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0 pe-5">
                <li className="nav-item">
                  <button type="button" className="btn btn-custom btn-lg mx-auto" hidden>
                    Large button
                  </button>
                </li>
              </ul>
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item"></li>
              </ul>
              <ul className="navbar-nav">
                <li className="nav-item dropdown">
                  <a
                    className="nav-link m-0 p-0"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    <div className="row p-0 m-0 d-flex flex-nowrap justify-content-center align-items-center w-100">
                      <div className="col-4 m-0 p-0 d-flex justify-content-center align-items-center" style={{ width: "45px" }}>
                        <svg width={38} height={38} viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <circle cx="17.5292" cy="17.5292" r="17.5292" fill="#8DC63F" />
                          <mask id="mask0_222_7594" style={{ maskType: "alpha" }} maskUnits="userSpaceOnUse" x={0} y={0} width={36} height={36}>
                            <circle cx="17.7011" cy="17.5292" r="17.5292" fill="#FFC145" />
                          </mask>
                          <g mask="url(#mask0_222_7594)">
                            <mask id="mask1_222_7594" style={{ maskType: "alpha" }} maskUnits="userSpaceOnUse" x={0} y={0} width={36} height={36}>
                              <circle cx="17.7011" cy="17.5292" r="17.5292" fill="#FFC145" />
                            </mask>
                            <g mask="url(#mask1_222_7594)">
                              <path d="M28.5328 37.1468V33.566C28.5328 31.6666 27.9622 29.845 26.9464 28.5019C25.9306 27.1588 24.5529 26.4043 23.1164 26.4043H12.2836C10.8471 26.4043 9.46939 27.1588 8.45362 28.5019C7.43784 29.845 6.86719 31.6666 6.86719 33.566V37.1468" fill="#4F4F4F" />
                              <path d="M17.6996 20.9871C20.691 20.9871 23.116 18.5621 23.116 15.5707C23.116 12.5793 20.691 10.1543 17.6996 10.1543C14.7082 10.1543 12.2832 12.5793 12.2832 15.5707C12.2832 18.5621 14.7082 20.9871 17.6996 20.9871Z" fill="white" />
                            </g>
                          </g>
                        </svg>
                      </div>
                      <div className="col-8 m-0 p-0 ms-1 h-100 flex-grow-1 d-flex flex-column justify-content-center align-items-start" style={{ height: '50px' }}>
                        <div className='m-0 p-0 dm-600 text-nowrap text-dark' style={{ fontSize: "14px" }}>Pema Dorji</div>
                        <div className='m-0 p-0 dm-500' style={{ fontSize: "12px", color: '#AEAEAE' }}>Admin</div>
                      </div>
                    </div>
                  </a>
                  <ul className="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                    <li><a className="dropdown-item" href="#">Action</a></li>
                    <li><a className="dropdown-item" href="#">Another action</a></li>
                    <li><hr className="dropdown-divider" /></li>
                    <li><a className="dropdown-item" href="#">Something else here</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div className="container mt-5">
          <div className="row">
            <div className="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
              <div className="rounded p-4 shadow">
                <h1 className="fs-2 fw-bold text-center py-2 mt-4">Create Role</h1>
                <input className="form-control mb-4" style={{ borderRadius: "5px"}} placeholder="Role Name" />
                <select className="form-select mb-4">
                  <option value="" disabled selected>Operated by</option>
                  <option value="Option 1">Option 1</option>
                  <option value="Option 2">Option 2</option>
                  <option value="Option 3">Option 3</option>
                </select>
                <p className="fw-bold">Access to</p>
                <div className="form-check mb-2">
                  <input className="form-check-input" type="checkbox" id="studentLogs" />
                  <label className="form-check-label" htmlFor="studentLogs">Student Logs</label>
                </div>
                <div className="form-check mb-4">
                  <input className="form-check-input" type="checkbox" id="visitorLogs" />
                  <label className="form-check-label" htmlFor="visitorLogs">Visitor Logs</label>
                </div>
                <button className="btn btn-custom btn-block">Create</button>
              </div>
            </div>
          </div>
      </div>

      </div>
    </div>
  );
}

export default CreateRole;
