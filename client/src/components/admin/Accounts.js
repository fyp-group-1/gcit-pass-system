import React from 'react'
import Sidebar from './sidebar/Sidebar';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './navbar.css'
function Accounts({ setIsAuthenticated }) {
  // const handleLogout = () => {
  //   // Clear the access token from local storage
  //   localStorage.removeItem('access_token');
  //   setIsAuthenticated(localStorage.removeItem('access_token'))
  // };

  return (
    <div class="row vh-100 vw-100">
      {console.log(localStorage.getItem('access_token'))}
      <div class="col-3">
        <Sidebar setIsAuthenticated={setIsAuthenticated} ></Sidebar>
      </div>
      {/* main container */}
      <div class="col-9 container d-flex flex-column ">
        <nav class="navbar navbar-expand-lg navbar-light bg-white p-4 row">
          <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          <div class="collapse navbar-collapse bg-" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 pe-5">
              <li class="nav-item">
                <button type="button" class="btn btn-custom btn-lg mx-auto" hidden>Large button</button>
              </li>
            </ul>
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
            </li>
          </ul>
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link m-0 p-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <div className="row p-0 m-0 d-flex flex-nowrap justify-content-center align-items-center w-100 ">
                  <div className="col-4 m-0 p-0 d-flex justify-content-center align-items-center" style={{width: "45px"}}>
                      <svg
              width={38}
              height={38}
              viewBox="0 0 36 36"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <circle cx="17.5292" cy="17.5292" r="17.5292" fill="#8DC63F" />
              <mask
                id="mask0_222_7594"
                style={{ maskType: "alpha" }}
                maskUnits="userSpaceOnUse"
                x={0}
                y={0}
                width={36}
                height={36}
              >
                <circle cx="17.7011" cy="17.5292" r="17.5292" fill="#FFC145" />
              </mask>
              <g mask="url(#mask0_222_7594)">
                <mask
                  id="mask1_222_7594"
                  style={{ maskType: "alpha" }}
                  maskUnits="userSpaceOnUse"
                  x={0}
                  y={0}
                  width={36}
                  height={36}
                >
                  <circle cx="17.7011" cy="17.5292" r="17.5292" fill="#FFC145" />
                </mask>
                <g mask="url(#mask1_222_7594)">
                  <path
                    d="M28.5328 37.1468V33.566C28.5328 31.6666 27.9622 29.845 26.9464 28.5019C25.9306 27.1588 24.5529 26.4043 23.1164 26.4043H12.2836C10.8471 26.4043 9.46939 27.1588 8.45362 28.5019C7.43784 29.845 6.86719 31.6666 6.86719 33.566V37.1468"
                    fill="#4F4F4F"
                  />
                  <path
                    d="M17.6996 20.9871C20.691 20.9871 23.116 18.5621 23.116 15.5707C23.116 12.5793 20.691 10.1543 17.6996 10.1543C14.7082 10.1543 12.2832 12.5793 12.2832 15.5707C12.2832 18.5621 14.7082 20.9871 17.6996 20.9871Z"
                    fill="white"
                  />
                </g>
              </g>
                      </svg>
                  </div>
                  <div className="col-8 m-0 p-0 ms-1 h-100 flex-grow-1 d-flex flex-column justify-content-center align-items-start" style={{height: '50px'}}>
                      <div className=' m-0 p-0 dm-600 text-nowrap text-dark' style={{fontSize: "14px"}}>Pema Dorji</div>
                      <div className=' m-0 p-0 dm-500 '  style={{fontSize: "12px", color: '#AEAEAE'}}>Admin</div>
                  </div>
                </div>
              </a>
              <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#">Action</a></li>
                <li><a class="dropdown-item" href="#">Another action</a></li>
                <li><hr class="dropdown-divider" /></li>
                <li><a class ="dropdown-item" href="#">Something else here</a></li>
              </ul>
            </li>
          </ul>
        </div>
          </div>
        </nav> 
        {/* MAIN CONTAINER */}
        
        <div className="row d-flex justify-content-between p-5 ">
        <div className="col-md-3 col">
          <h1 className='dm-600 fs-3'>Accounts <span className='dm-600 fs-5 text-muted'> &gt; List</span></h1>
        </div>
            <div className="bot-container my-5  p-5 shadow-lg rounded">
              <div class="container border-bottom border-2 mb-2 mt-2">
                <div class="row">
                  <div class="col">
                    <div class="d-flex justify-content-start">
                      <p class="m-2 text-orange fw-bold">All(15)</p>
                      <p class="m-2 text-muted">Admin(15)</p>
                      <p class="m-2 text-muted">Gatekeeper(15)</p>
                      <p class="m-2 text-muted">Counselor(15)</p>
                      <p class="m-2 text-muted">SSO(15)</p>
                      <p class="m-2 text-muted">HR(15)</p>
                    </div>
                  </div>
                </div>
              </div>
            <div className="table-responsive">
            <table class="table table-striped table-xl bg-white">
              <thead>
                <tr>
                  <th scope="col" className='dm-600'>Full Name</th>
                  <th scope="col" className='dm-600'>Email</th>
                  <th scope="col" className='dm-600'>Role</th>
                  <th scope="col" className='dm-600'>Access</th>
                  <th scope="col" className='dm-600'>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr className='text-muted '>
                  <td>Pema Dorji</td>
                  <td>sonamwangmo.gcit@rub.edu.bt</td>
                  <td>Lecturer</td>
                  <td>17002173</td>
                  <td>
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20" fill="none">
                    <path d="M14 20H4C2.89543 20 2 19.1046 2 18V5H0V3H4V2C4 0.89543 4.89543 0 6 0H12C13.1046 0 14 0.89543 14 2V3H18V5H16V18C16 19.1046 15.1046 20 14 20ZM4 5V18H14V5H4ZM6 2V3H12V2H6ZM12 16H10V7H12V16ZM8 16H6V7H8V16Z" fill="#2E3A59"/>
                    </svg>
                    <svg className="ms-3"xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                    <path d="M1.13107 13.5767C0.932373 13.5764 0.742955 13.4926 0.609031 13.3458C0.472638 13.2002 0.404864 13.0033 0.422739 12.8046L0.596281 10.8964L8.6132 2.8823L11.1186 5.38696L3.10378 13.4003L1.19553 13.5739C1.17357 13.576 1.15161 13.5767 1.13107 13.5767ZM11.6187 4.88617L9.11399 2.38151L10.6164 0.879131C10.7492 0.746123 10.9295 0.671387 11.1175 0.671387C11.3055 0.671387 11.4858 0.746123 11.6187 0.879131L13.121 2.38151C13.254 2.51437 13.3288 2.69465 13.3288 2.88265C13.3288 3.07065 13.254 3.25094 13.121 3.3838L11.6194 4.88546L11.6187 4.88617Z" fill="#2E3A59"/>
                    </svg>
                  </td>
                </tr>
                <tr className='text-muted'>
                  <td>Pema Dorji</td>
                  <td>sonamwangmo.gcit@rub.edu.bt</td>
                  <td>Associate Lecturer</td>
                  <td>17602173</td>
                  <td>
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20" fill="none">
                    <path d="M14 20H4C2.89543 20 2 19.1046 2 18V5H0V3H4V2C4 0.89543 4.89543 0 6 0H12C13.1046 0 14 0.89543 14 2V3H18V5H16V18C16 19.1046 15.1046 20 14 20ZM4 5V18H14V5H4ZM6 2V3H12V2H6ZM12 16H10V7H12V16ZM8 16H6V7H8V16Z" fill="#2E3A59"/>
                    </svg>
                    <svg className="ms-3"xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                    <path d="M1.13107 13.5767C0.932373 13.5764 0.742955 13.4926 0.609031 13.3458C0.472638 13.2002 0.404864 13.0033 0.422739 12.8046L0.596281 10.8964L8.6132 2.8823L11.1186 5.38696L3.10378 13.4003L1.19553 13.5739C1.17357 13.576 1.15161 13.5767 1.13107 13.5767ZM11.6187 4.88617L9.11399 2.38151L10.6164 0.879131C10.7492 0.746123 10.9295 0.671387 11.1175 0.671387C11.3055 0.671387 11.4858 0.746123 11.6187 0.879131L13.121 2.38151C13.254 2.51437 13.3288 2.69465 13.3288 2.88265C13.3288 3.07065 13.254 3.25094 13.121 3.3838L11.6194 4.88546L11.6187 4.88617Z" fill="#2E3A59"/>
                    </svg>
                  </td>
                </tr>
                <tr className='text-muted'>
                  <td>Pema Dorji</td>
                  <td>sonamwangmo.gcit@rub.edu.bt</td>
                  <td>Lecturer</td>
                  <td>17602173</td>
                  <td>
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20" fill="none">
                    <path d="M14 20H4C2.89543 20 2 19.1046 2 18V5H0V3H4V2C4 0.89543 4.89543 0 6 0H12C13.1046 0 14 0.89543 14 2V3H18V5H16V18C16 19.1046 15.1046 20 14 20ZM4 5V18H14V5H4ZM6 2V3H12V2H6ZM12 16H10V7H12V16ZM8 16H6V7H8V16Z" fill="#2E3A59"/>
                    </svg>
                    <svg className="ms-3"xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                    <path d="M1.13107 13.5767C0.932373 13.5764 0.742955 13.4926 0.609031 13.3458C0.472638 13.2002 0.404864 13.0033 0.422739 12.8046L0.596281 10.8964L8.6132 2.8823L11.1186 5.38696L3.10378 13.4003L1.19553 13.5739C1.17357 13.576 1.15161 13.5767 1.13107 13.5767ZM11.6187 4.88617L9.11399 2.38151L10.6164 0.879131C10.7492 0.746123 10.9295 0.671387 11.1175 0.671387C11.3055 0.671387 11.4858 0.746123 11.6187 0.879131L13.121 2.38151C13.254 2.51437 13.3288 2.69465 13.3288 2.88265C13.3288 3.07065 13.254 3.25094 13.121 3.3838L11.6194 4.88546L11.6187 4.88617Z" fill="#2E3A59"/>
                    </svg>
                  </td>
                </tr>
                <tr className='text-muted'>
                  <td>Pema Dorji</td>
                  <td>sonamwangmo.gcit@rub.edu.bt</td>
                  <td>Associate Lecturer</td>
                  <td>17602173</td>
                  <td>
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20" fill="none">
                    <path d="M14 20H4C2.89543 20 2 19.1046 2 18V5H0V3H4V2C4 0.89543 4.89543 0 6 0H12C13.1046 0 14 0.89543 14 2V3H18V5H16V18C16 19.1046 15.1046 20 14 20ZM4 5V18H14V5H4ZM6 2V3H12V2H6ZM12 16H10V7H12V16ZM8 16H6V7H8V16Z" fill="#2E3A59"/>
                    </svg>
                    <svg className="ms-3"xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                    <path d="M1.13107 13.5767C0.932373 13.5764 0.742955 13.4926 0.609031 13.3458C0.472638 13.2002 0.404864 13.0033 0.422739 12.8046L0.596281 10.8964L8.6132 2.8823L11.1186 5.38696L3.10378 13.4003L1.19553 13.5739C1.17357 13.576 1.15161 13.5767 1.13107 13.5767ZM11.6187 4.88617L9.11399 2.38151L10.6164 0.879131C10.7492 0.746123 10.9295 0.671387 11.1175 0.671387C11.3055 0.671387 11.4858 0.746123 11.6187 0.879131L13.121 2.38151C13.254 2.51437 13.3288 2.69465 13.3288 2.88265C13.3288 3.07065 13.254 3.25094 13.121 3.3838L11.6194 4.88546L11.6187 4.88617Z" fill="#2E3A59"/>
                    </svg>
                  </td>
                </tr>
              </tbody>
              <br></br>
            </table>
            <div>
            <nav aria-label="Page navigation example">
              <ul className="pagination text-end d-flex justify-content-end">
                <li class="page-item"><a class="page-link text-dark" href="#">Previous</a></li>
                <li class="page-item"><a class="page-link text-dark" href="#">1</a></li>
                <li class="page-item"><a class="page-link text-dark" href="#">2</a></li>
                <li class="page-item"><a class="page-link text-dark" href="#">3</a></li>
                <li class="page-item"><a class="page-link text-dark" href="#">Next</a></li>
              </ul>
            </nav>
          </div>
          </div>      
          </div>
            
            
            
            </div>
        
      </div>
    </div>
    
  )
}

export default Accounts;