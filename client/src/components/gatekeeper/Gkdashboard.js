import React from 'react'
import Sidebar from './sidebar/Sidebar';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './navbar.css'
function Gkdashboard({ setIsAuthenticated }) {
  // const handleLogout = () => {
  //   // Clear the access token from local storage
  //   localStorage.removeItem('access_token');
  //   setIsAuthenticated(localStorage.removeItem('access_token'))
  // };

  return (
    <div class="row vh-100 vw-100 ">
      {console.log(localStorage.getItem('access_token'))}
      <div class="col-3">
        <Sidebar setIsAuthenticated ={setIsAuthenticated} ></Sidebar>
      </div>
      {/* main container */}
      <div class="col-9 container vh-100 d-flex flex-column ">
        <nav class="navbar navbar-expand-lg navbar-light bg-white p-4 row">
          <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          <div class="collapse navbar-collapse bg-" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 pe-5">
              <li class="nav-item">

                <button type="button" class="btn btn-custom btn-lg mx-auto" hidden>Large button</button>
              </li>
            </ul>
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
            
              <button type="button" class="btn btn-custom btn-lg mx-auto"> 
              <svg
  width={30}
  height={30}
  viewBox="0 0 30 30"
  fill="none"
  xmlns="http://www.w3.org/2000/svg"
>
  <path
    d="M5.28828 5.4001H12.4883V12.6001H5.28828V5.4001ZM24.4883 5.4001V12.6001H17.2883V5.4001H24.4883ZM17.2883 18.6001H19.6883V16.2001H17.2883V13.8001H19.6883V16.2001H22.0883V13.8001H24.4883V16.2001H22.0883V18.6001H24.4883V22.2001H22.0883V24.6001H19.6883V22.2001H16.0883V24.6001H13.6883V19.8001H17.2883V18.6001ZM19.6883 18.6001V22.2001H22.0883V18.6001H19.6883ZM5.28828 24.6001V17.4001H12.4883V24.6001H5.28828ZM7.68828 7.8001V10.2001H10.0883V7.8001H7.68828ZM19.6883 7.8001V10.2001H22.0883V7.8001H19.6883ZM7.68828 19.8001V22.2001H10.0883V19.8001H7.68828ZM5.28828 13.8001H7.68828V16.2001H5.28828V13.8001ZM11.2883 13.8001H16.0883V18.6001H13.6883V16.2001H11.2883V13.8001ZM13.6883 7.8001H16.0883V12.6001H13.6883V7.8001ZM2.88828 3.0001V7.8001H0.488281V3.0001C0.488281 2.36358 0.741138 1.75313 1.19123 1.30304C1.64131 0.852954 2.25176 0.600098 2.88828 0.600098L7.68828 0.600098V3.0001H2.88828ZM26.8883 0.600098C27.5248 0.600098 28.1353 0.852954 28.5853 1.30304C29.0354 1.75313 29.2883 2.36358 29.2883 3.0001V7.8001H26.8883V3.0001H22.0883V0.600098H26.8883ZM2.88828 22.2001V27.0001H7.68828V29.4001H2.88828C2.25176 29.4001 1.64131 29.1472 1.19123 28.6972C0.741138 28.2471 0.488281 27.6366 0.488281 27.0001V22.2001H2.88828ZM26.8883 27.0001V22.2001H29.2883V27.0001C29.2883 27.6366 29.0354 28.2471 28.5853 28.6972C28.1353 29.1472 27.5248 29.4001 26.8883 29.4001H22.0883V27.0001H26.8883Z"
    fill="white"
  />
            </svg>
        <span className='ms-2 dm-500'> Scan QR  </span> </button>
            </li>
          </ul>
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link m-0 p-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <div className="row p-0 m-0 d-flex flex-nowrap justify-content-center align-items-center w-100 ">
                  <div className="col-4 m-0 p-0 d-flex justify-content-center align-items-center" style={{width: "45px"}}>
                      <svg
              width={38}
              height={38}
              viewBox="0 0 36 36"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <circle cx="17.5292" cy="17.5292" r="17.5292" fill="#8DC63F" />
              <mask
                id="mask0_222_7594"
                style={{ maskType: "alpha" }}
                maskUnits="userSpaceOnUse"
                x={0}
                y={0}
                width={36}
                height={36}
              >
                <circle cx="17.7011" cy="17.5292" r="17.5292" fill="#FFC145" />
              </mask>
              <g mask="url(#mask0_222_7594)">
                <mask
                  id="mask1_222_7594"
                  style={{ maskType: "alpha" }}
                  maskUnits="userSpaceOnUse"
                  x={0}
                  y={0}
                  width={36}
                  height={36}
                >
                  <circle cx="17.7011" cy="17.5292" r="17.5292" fill="#FFC145" />
                </mask>
                <g mask="url(#mask1_222_7594)">
                  <path
                    d="M28.5328 37.1468V33.566C28.5328 31.6666 27.9622 29.845 26.9464 28.5019C25.9306 27.1588 24.5529 26.4043 23.1164 26.4043H12.2836C10.8471 26.4043 9.46939 27.1588 8.45362 28.5019C7.43784 29.845 6.86719 31.6666 6.86719 33.566V37.1468"
                    fill="#4F4F4F"
                  />
                  <path
                    d="M17.6996 20.9871C20.691 20.9871 23.116 18.5621 23.116 15.5707C23.116 12.5793 20.691 10.1543 17.6996 10.1543C14.7082 10.1543 12.2832 12.5793 12.2832 15.5707C12.2832 18.5621 14.7082 20.9871 17.6996 20.9871Z"
                    fill="white"
                  />
                </g>
              </g>
                      </svg>
                  </div>
                  <div className="col-8 m-0 p-0 ms-1 h-100 flex-grow-1 d-flex flex-column justify-content-center align-items-start" style={{height: '50px'}}>
                      <div className=' m-0 p-0 dm-600 text-nowrap text-dark' style={{fontSize: "14px"}}>Thinley Dorji</div>
                      <div className=' m-0 p-0 dm-500 '  style={{fontSize: "12px", color: '#AEAEAE'}}>Gatekeeper</div>
                  </div>
                </div>
              </a>
              <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                <li><button class="dropdown-item btn">Change Password</button></li>
              </ul>
            </li>
          </ul>
        </div>
          </div>
        </nav> 
        {/* MAIN CONTAINER */}
        <div className="container scrollable-div w-100 100vh py-5" style={{overflow: "auto" }}>
          <div class="row row-cols-1 row-cols-md-3 g-4">
            <div class="col-lg-3 col-md-4 col-sm-6 col-12 mx-auto">
              <div class=" h-100 w-100">
              <div className='col-lg-3 col-md-4 col-sm-6 col-12 mx-auto w-100 h-100 d-flex justify-content-center align-items-center'>
                <div className='shadow-lg d-flex flex-column align-items-center w' style={{ width: '250px', height: '300px', margin: '10px', borderRadius: "25px", }}>
                  <h1 style={{ fontSize: "85px", color: "#828282",paddingTop:"35px"}}>3</h1>
                  <div className='row' style={{ margin: '0', padding: '0' }}>
                  <div className='col'>
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="19" viewBox="0 0 20 19" fill="none">
                    <path d="M20 19H0V9C0 7.89543 0.89543 7 2 7H6V2C6 0.89543 6.89543 0 8 0H12C13.1046 0 14 0.89543 14 2V5H18C19.1046 5 20 5.89543 20 7V19ZM14 7V17H18V7H14ZM8 2V17H12V2H8ZM2 9V17H6V9H2Z" fill="#BDBDBD" />
                  </svg>
                  </div>
                  <div className='col' style={{ margin: '0', padding: '0' }}>
                  <p style={{ color: "#cfcfcf", paddingTop:"4px" }} className='dm-600'>Total</p>
                  </div>
                  </div>
                  <div className="second-svg" style={{paddingTop:"23px" }}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="23" viewBox="0 0 20 18" fill="none">
                      <path d="M7 0C4.23858 0 2 2.23858 2 5C2 7.76142 4.23858 10 7 10C9.76142 10 12 7.76142 12 5C12 2.23858 9.76142 0 7 0ZM4 5C4 3.34315 5.34315 2 7 2C8.65685 2 10 3.34315 10 5C10 6.65685 8.65685 8 7 8C5.34315 8 4 6.65685 4 5Z" fill="#2E3A59" />
                      <path d="M14.9084 5.21828C14.6271 5.07484 14.3158 5.00006 14 5.00006V3.00006C14.6316 3.00006 15.2542 3.14956 15.8169 3.43645C15.8789 3.46805 15.9399 3.50121 16 3.5359C16.4854 3.81614 16.9072 4.19569 17.2373 4.65055C17.6083 5.16172 17.8529 5.75347 17.9512 6.37737C18.0496 7.00126 17.9987 7.63958 17.8029 8.24005C17.6071 8.84053 17.2719 9.38611 16.8247 9.83213C16.3775 10.2782 15.8311 10.6119 15.2301 10.8062C14.6953 10.979 14.1308 11.037 13.5735 10.9772C13.5046 10.9698 13.4357 10.9606 13.367 10.9496C12.7438 10.8497 12.1531 10.6038 11.6431 10.2319L11.6421 10.2311L12.821 8.61557C13.0761 8.80172 13.3717 8.92477 13.6835 8.97474C13.9953 9.02471 14.3145 9.00014 14.615 8.90302C14.9155 8.80591 15.1887 8.63903 15.4123 8.41602C15.6359 8.19302 15.8035 7.92024 15.9014 7.62001C15.9993 7.31978 16.0247 7.00063 15.9756 6.68869C15.9264 6.37675 15.8041 6.08089 15.6186 5.82531C15.4331 5.56974 15.1898 5.36172 14.9084 5.21828Z" fill="#2E3A59" />
                      <path d="M17.9981 18C17.9981 17.475 17.8947 16.9551 17.6938 16.47C17.4928 15.9849 17.1983 15.5442 16.8271 15.1729C16.4558 14.8017 16.0151 14.5072 15.53 14.3062C15.0449 14.1053 14.525 14.0019 14 14.0019V12C14.6821 12 15.3584 12.1163 16 12.3431C16.0996 12.3783 16.1983 12.4162 16.2961 12.4567C17.0241 12.7583 17.6855 13.2002 18.2426 13.7574C18.7998 14.3145 19.2417 14.9759 19.5433 15.7039C19.5838 15.8017 19.6217 15.9004 19.6569 16C19.8837 16.6416 20 17.3179 20 18H17.9981Z" fill="#2E3A59" />
                      <path d="M14 18H12C12 15.2386 9.76142 13 7 13C4.23858 13 2 15.2386 2 18H0C0 14.134 3.13401 11 7 11C10.866 11 14 14.134 14 18Z" fill="#2E3A59" />
                    </svg>
                  </div>
                  <h1 style={{ fontSize: "22px", fontFamily: 'DM Sans, sans-serif',paddingTop:"10px" }} className='dm-500'>Visitor Expected</h1>
                </div>
              </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-12 mx-auto">
              <div class=" h-100 w-100">
              <div className='col-lg-3 col-md-4 col-sm-6 col-12 mx-auto w-100 h-100 d-flex justify-content-center align-items-center'>
                <div className='shadow-lg d-flex flex-column align-items-center w' style={{ width: '250px', height: '300px', margin: '10px', borderRadius: "25px", }}>
                <h1 style={{ fontSize: "85px", color: "#828282" ,paddingTop:"35px" }} className='dm-500'>4</h1>
                  <div className='row' style={{ margin: '0', padding: '0' }}>
                  <div className='col'>
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="19" viewBox="0 0 20 19" fill="none">
                    <path d="M20 19H0V9C0 7.89543 0.89543 7 2 7H6V2C6 0.89543 6.89543 0 8 0H12C13.1046 0 14 0.89543 14 2V5H18C19.1046 5 20 5.89543 20 7V19ZM14 7V17H18V7H14ZM8 2V17H12V2H8ZM2 9V17H6V9H2Z" fill="#BDBDBD" />
                  </svg>
                  </div>
                  <div className='col' style={{ margin: '0', padding: '0' }}>
                  <p style={{ color: "#cfcfcf", paddingTop:"4px"}} className='dm-600'>Total</p>
                  </div>
                  </div>
                  <div className="second-svg" style={{paddingTop:"23px"}}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="23" viewBox="0 0 20 18" fill="none">
                      <path d="M7 0C4.23858 0 2 2.23858 2 5C2 7.76142 4.23858 10 7 10C9.76142 10 12 7.76142 12 5C12 2.23858 9.76142 0 7 0ZM4 5C4 3.34315 5.34315 2 7 2C8.65685 2 10 3.34315 10 5C10 6.65685 8.65685 8 7 8C5.34315 8 4 6.65685 4 5Z" fill="#2E3A59" />
                      <path d="M14.9084 5.21828C14.6271 5.07484 14.3158 5.00006 14 5.00006V3.00006C14.6316 3.00006 15.2542 3.14956 15.8169 3.43645C15.8789 3.46805 15.9399 3.50121 16 3.5359C16.4854 3.81614 16.9072 4.19569 17.2373 4.65055C17.6083 5.16172 17.8529 5.75347 17.9512 6.37737C18.0496 7.00126 17.9987 7.63958 17.8029 8.24005C17.6071 8.84053 17.2719 9.38611 16.8247 9.83213C16.3775 10.2782 15.8311 10.6119 15.2301 10.8062C14.6953 10.979 14.1308 11.037 13.5735 10.9772C13.5046 10.9698 13.4357 10.9606 13.367 10.9496C12.7438 10.8497 12.1531 10.6038 11.6431 10.2319L11.6421 10.2311L12.821 8.61557C13.0761 8.80172 13.3717 8.92477 13.6835 8.97474C13.9953 9.02471 14.3145 9.00014 14.615 8.90302C14.9155 8.80591 15.1887 8.63903 15.4123 8.41602C15.6359 8.19302 15.8035 7.92024 15.9014 7.62001C15.9993 7.31978 16.0247 7.00063 15.9756 6.68869C15.9264 6.37675 15.8041 6.08089 15.6186 5.82531C15.4331 5.56974 15.1898 5.36172 14.9084 5.21828Z" fill="#2E3A59" />
                      <path d="M17.9981 18C17.9981 17.475 17.8947 16.9551 17.6938 16.47C17.4928 15.9849 17.1983 15.5442 16.8271 15.1729C16.4558 14.8017 16.0151 14.5072 15.53 14.3062C15.0449 14.1053 14.525 14.0019 14 14.0019V12C14.6821 12 15.3584 12.1163 16 12.3431C16.0996 12.3783 16.1983 12.4162 16.2961 12.4567C17.0241 12.7583 17.6855 13.2002 18.2426 13.7574C18.7998 14.3145 19.2417 14.9759 19.5433 15.7039C19.5838 15.8017 19.6217 15.9004 19.6569 16C19.8837 16.6416 20 17.3179 20 18H17.9981Z" fill="#2E3A59" />
                      <path d="M14 18H12C12 15.2386 9.76142 13 7 13C4.23858 13 2 15.2386 2 18H0C0 14.134 3.13401 11 7 11C10.866 11 14 14.134 14 18Z" fill="#2E3A59" />
                    </svg>
                  </div>
                  <h1 style={{ fontSize: "22px", fontFamily: 'DM Sans, sans-serif',paddingTop:"10px" }}>Off-campus count</h1>
                </div>
              </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-12 mx-auto">
              <div class=" h-100 w-100">
              <div className='col-lg-3 col-md-4 col-sm-6 col-12 mx-auto w-100 h-100 d-flex justify-content-center align-items-center'>
                <div className='shadow-lg d-flex flex-column align-items-center w' style={{ width: '250px', height: '300px', margin: '10px', borderRadius: "25px", }}>
                <h1 style={{ fontSize: "85px", color: "#828282" ,paddingTop:"35px"}}>378</h1>
                  <div className='row' style={{ margin: '0', padding: '0' }}>
                  <div className='col'>
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="19" viewBox="0 0 20 19" fill="none">
                    <path d="M20 19H0V9C0 7.89543 0.89543 7 2 7H6V2C6 0.89543 6.89543 0 8 0H12C13.1046 0 14 0.89543 14 2V5H18C19.1046 5 20 5.89543 20 7V19ZM14 7V17H18V7H14ZM8 2V17H12V2H8ZM2 9V17H6V9H2Z" fill="#BDBDBD" />
                  </svg>
                  </div>
                  <div className='col' style={{ margin: '0', padding: '0' }}>
                  <p style={{ color: "#cfcfcf", paddingTop:"4px" }} className='dm-600'>Total</p>
                  </div>
                  </div>
                  <div className="second-svg" style={{paddingTop:"23px" }}>
                  <svg xmlns="http://www.w3.org/2000/svg" width="35" height="23" viewBox="0 0 20 15" fill="none">
                  <path d="M2 15H0C0 11.6863 2.68629 9 6 9C9.31371 9 12 11.6863 12 15H10C10 12.7909 8.20914 11 6 11C3.79086 11 2 12.7909 2 15ZM18.294 11.706L16 9.413L13.707 11.706L12.293 10.292L14.585 8L12.293 5.707L13.707 4.293L16 6.586L18.293 4.293L19.707 5.707L17.414 8L19.707 10.293L18.294 11.706ZM6 8C3.79086 8 2 6.20914 2 4C2 1.79086 3.79086 0 6 0C8.20914 0 10 1.79086 10 4C9.99724 6.208 8.208 7.99725 6 8ZM6 2C4.9074 2.00111 4.01789 2.87885 4.00223 3.97134C3.98658 5.06383 4.85057 5.9667 5.94269 5.99912C7.03481 6.03153 7.95083 5.1815 8 4.09V4.49V4C8 2.89543 7.10457 2 6 2Z" fill="#2E3A59"/>
                  </svg>
                    </div>
                  <h1 style={{ fontSize: "22px", fontFamily: 'DM Sans, sans-serif',paddingTop:"7px" }}>In-campus count</h1>
                </div>
              </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-12 mx-auto">
              <div class=" h-100 w-100">
              <div className='col-lg-3 col-md-4 col-sm-6 col-12 mx-auto w-100 h-100 d-flex justify-content-center align-items-center'>
                <div className='shadow-lg d-flex flex-column align-items-center w' style={{ width: '250px', height: '300px', margin: '10px', borderRadius: "25px", }}>
                <h1 style={{ fontSize: "85px", color: "#828282" ,paddingTop:"35px"}}>3</h1>
                  <div className='row' style={{ margin: '0', padding: '0' }}>
                  <div className='col'>
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="19" viewBox="0 0 20 19" fill="none">
                    <path d="M20 19H0V9C0 7.89543 0.89543 7 2 7H6V2C6 0.89543 6.89543 0 8 0H12C13.1046 0 14 0.89543 14 2V5H18C19.1046 5 20 5.89543 20 7V19ZM14 7V17H18V7H14ZM8 2V17H12V2H8ZM2 9V17H6V9H2Z" fill="#BDBDBD" />
                  </svg>
                  </div>
                  <div className='col' style={{ margin: '0', padding: '0' }}>
                  <p style={{ color: "#cfcfcf", paddingTop:"4px" }} className='dm-600'>Total</p>
                  </div>
                  </div>
                  <div className="second-svg" style={{paddingTop:"25px"}}>
                  <svg xmlns="http://www.w3.org/2000/svg" width="35" height="23" viewBox="0 0 19 17" fill="none">
                  <path d="M2 17.0012H0C0 13.6875 2.68629 11.0012 6 11.0012C9.31371 11.0012 12 13.6875 12 17.0012H10C10 14.7921 8.20914 13.0012 6 13.0012C3.79086 13.0012 2 14.7921 2 17.0012ZM16.364 13.3642L14.95 11.9502C16.2629 10.6375 17.0005 8.85687 17.0005 7.00023C17.0005 5.14359 16.2629 3.36301 14.95 2.05023L16.364 0.63623C19.8781 4.15084 19.8781 9.84862 16.364 13.3632V13.3642ZM13.535 10.5362L12.121 9.12023C13.2908 7.94896 13.2908 6.0515 12.121 4.88023L13.535 3.46323C15.4876 5.41584 15.4876 8.58162 13.535 10.5342V10.5362ZM6 10.0002C3.79086 10.0002 2 8.20937 2 6.00023C2 3.79109 3.79086 2.00023 6 2.00023C8.20914 2.00023 10 3.79109 10 6.00023C10 7.0611 9.57857 8.07851 8.82843 8.82866C8.07828 9.5788 7.06087 10.0002 6 10.0002ZM6 4.00023C4.9074 4.00134 4.01789 4.87908 4.00223 5.97157C3.98658 7.06406 4.85057 7.96693 5.94269 7.99935C7.03481 8.03176 7.95083 7.18173 8 6.09023V6.49023V6.00023C8 4.89566 7.10457 4.00023 6 4.00023Z" fill="#2E3A59"/>
                  </svg>
                  </div>
                  <h1 style={{ fontSize: "22px", fontFamily: 'DM Sans, sans-serif',paddingTop:"7px" }}>Defaulted Visitor </h1>
                </div>
              </div>
              </div>
            </div>
          </div>
        {/* BOTTOM CONTAINTER */}

            

          <div className="bot-container my-5  p-5 shadow-lg rounded">
            <div className="row my-4">
              <div className="col">
                <span className=''> <span className="me-2 dm-600 fs-4 ">
                Expected Visitor
                </span>
                  <svg width="30" height="30" viewBox="0 0 17 23" fill="none" className='mb-2' xmlns="http://www.w3.org/2000/svg">
                  <path d="M5.66667 2.3H5.07167C4.88278 2.3 4.70333 2.31917 4.53333 2.3575C5.61 3.6225 6.44564 5.03623 7.04027 6.5987C7.63489 8.16117 7.93258 9.79493 7.93333 11.5C7.93333 13.2058 7.63564 14.84 7.04027 16.4024C6.44489 17.9649 5.60924 19.3783 4.53333 20.6425C4.70333 20.6808 4.88278 20.7 5.07167 20.7H5.66667C8.17889 20.7 10.3182 19.8041 12.0847 18.0124C13.8512 16.2207 14.7341 14.0499 14.7333 11.5C14.7333 8.95083 13.8505 6.78002 12.0847 4.98755C10.319 3.19508 8.17964 2.29923 5.66667 2.3ZM5.66667 0C7.23444 0 8.70778 0.302067 10.0867 0.9062C11.4656 1.51033 12.665 2.32952 13.685 3.36375C14.705 4.39875 15.5127 5.61583 16.1081 7.015C16.7034 8.41417 17.0008 9.90917 17 11.5C17 13.0908 16.7023 14.5858 16.1069 15.985C15.5116 17.3842 14.7042 18.6012 13.685 19.6362C12.665 20.6712 11.4656 21.4908 10.0867 22.0949C8.70778 22.6991 7.23444 23.0008 5.66667 23C4.66556 23 3.68787 22.8704 2.7336 22.6113C1.77933 22.3522 0.868133 21.9642 0 21.4475C1.75667 20.4125 3.14047 19.0133 4.1514 17.25C5.16233 15.4867 5.66742 13.57 5.66667 11.5C5.66667 9.43 5.1612 7.51333 4.15027 5.75C3.13933 3.98667 1.75591 2.5875 0 1.5525C0.868889 1.035 1.78047 0.647067 2.73473 0.3887C3.689 0.130333 4.66631 0.000766667 5.66667 0Z" fill="black"/>
                  </svg>
                </span>
              </div>
              <div className="col-md-3 col">
                <form class="d-flex">
                <input class="form-control " type="search"  aria-label="Search" style={{borderTopRightRadius: "0px", borderBottomRightRadius: "0px", borderTopLeftRadius: "7px", borderBottomLeftRadius: "7px"}} />
                <button class="btn btn-custom " type="submit"  style={{borderTopRightRadius: "7px", borderBottomRightRadius: "7px", borderTopLeftRadius: "0px", borderBottomLeftRadius: "0px"}}>Search</button>
              </form>
              </div>
            </div>
            <div className="table-responsive">
            <table class="table table-striped table-xl bg-white">
              <thead>
                <tr>
                  <th scope="col" className='dm-600'>S/N</th>
                  <th scope="col" className='dm-600'>Pass ID</th>
                  <th scope="col" className='dm-600'>Full Name</th>
                  <th scope="col" className='dm-600'>Phone No</th>
                  <th scope="col" className='dm-600'>CID</th>
                  <th scope="col" className='dm-600'>Host</th>
                  <th scope="col" className='dm-600'>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kelden Norbu</td>
                  <td>17330827</td>
                  <td>10906002173</td>
                  <td>Sonam Wangmo</td>
                  <td><button className="btn btn-custom">Check In</button></td>
                </tr>
                <tr className='text-muted'>
                  <th scope="row">2</th>
                  <td>031</td>
                  <td>Kelden Norbu</td>
                  <td>17330827</td>
                  <td>10906002173</td>
                  <td>Sonam Wangmo</td>
                  <td><button className="btn btn-custom">Check In</button></td>
                </tr>
                <tr className='text-muted'>
                  <th scope="row">3</th>
                  <td>031</td>
                  <td>Kelden Norbu</td>
                  <td>17330827</td>
                  <td>10906002173</td>
                  <td>Sonam Wangmo</td>
                  <td><button className="btn btn-custom">Check In</button></td>
                </tr>
                <tr className='text-muted'>
                  <th scope="row">4</th>
                  <td>031</td>
                  <td>Kelden Norbu</td>
                  <td>17330827</td>
                  <td>10906002173</td>
                  <td>Sonam Wangmo</td>
                  <td><button className="btn btn-custom">Check In</button></td>
                </tr>
              </tbody>
            </table>
            </div>
            
          </div>
          <div className="bot-container my-5 vw-1 p-5 shadow-lg rounded">
            <div className="row my-4">
              <div className="col">
                <span className=''> <span className="me-2 dm-600 fs-4 ">
                Expected Visitor
                </span>
                  <svg width="30" height="30" viewBox="0 0 28 28" fill="none" className='mb-2' xmlns="http://www.w3.org/2000/svg">
<path d="M13.17 27.1682V23.7242C13.17 23.5035 13.2576 23.292 13.4136 23.136C13.5696 22.98 13.7811 22.8923 14.0017 22.8923C14.2224 22.8923 14.4339 22.98 14.5899 23.136C14.7459 23.292 14.8335 23.5035 14.8335 23.7242V27.1682C14.8335 27.3888 14.7459 27.6004 14.5899 27.7564C14.4339 27.9124 14.2224 28 14.0017 28C13.7811 28 13.5696 27.9124 13.4136 27.7564C13.2576 27.6004 13.17 27.3888 13.17 27.1682ZM22.7246 23.8968L20.2898 21.4632C20.2093 21.3867 20.1449 21.2949 20.1005 21.1931C20.056 21.0914 20.0324 20.9818 20.0309 20.8707C20.0295 20.7597 20.0503 20.6495 20.0921 20.5466C20.134 20.4438 20.196 20.3503 20.2745 20.2718C20.353 20.1933 20.4464 20.1313 20.5493 20.0895C20.6521 20.0476 20.7623 20.0268 20.8734 20.0283C20.9844 20.0297 21.094 20.0533 21.1958 20.0978C21.2975 20.1423 21.3893 20.2067 21.4658 20.2872L23.8982 22.7243C24.0345 22.8839 24.1054 23.089 24.0968 23.2987C24.0883 23.5084 24.0008 23.707 23.8519 23.8549C23.703 24.0028 23.5037 24.0889 23.294 24.096C23.0843 24.1031 22.8796 24.0307 22.7211 23.8933L22.7222 23.8945L22.7246 23.8968ZM4.103 23.8968C4.0254 23.8198 3.96381 23.7282 3.92177 23.6273C3.87974 23.5264 3.8581 23.4182 3.8581 23.3088C3.8581 23.1995 3.87974 23.0913 3.92177 22.9904C3.96381 22.8894 4.0254 22.7978 4.103 22.7208L6.53773 20.286C6.61419 20.2055 6.70601 20.1411 6.80775 20.0966C6.90949 20.0522 7.01911 20.0285 7.13014 20.0271C7.24116 20.0257 7.35135 20.0465 7.45421 20.0883C7.55706 20.1301 7.6505 20.1921 7.72902 20.2707C7.80753 20.3492 7.86953 20.4426 7.91136 20.5455C7.95319 20.6483 7.97401 20.7585 7.97258 20.8696C7.97115 20.9806 7.9475 21.0902 7.90304 21.192C7.85857 21.2937 7.79419 21.3855 7.71368 21.462L5.27895 23.8957C5.20195 23.9733 5.11035 24.0349 5.00944 24.0769C4.90853 24.1189 4.80029 24.1406 4.69097 24.1406C4.58165 24.1406 4.47342 24.1189 4.3725 24.0769C4.27159 24.0349 4.17999 23.9744 4.103 23.8968ZM7.37303 13.9977C7.37303 12.2392 8.07153 10.5528 9.31488 9.30943C10.5582 8.06603 12.2446 7.3675 14.0029 7.3675C15.7613 7.3675 17.4476 8.06603 18.691 9.30943C19.9343 10.5528 20.6328 12.2392 20.6328 13.9977C20.6328 15.7561 19.9343 17.4425 18.691 18.6859C17.4476 19.9293 15.7613 20.6278 14.0029 20.6278C12.2446 20.6278 10.5582 19.9293 9.31488 18.6859C8.07153 17.4425 7.37303 15.7561 7.37303 13.9977ZM9.03546 13.9977C9.02112 14.6592 9.13904 15.317 9.38229 15.9324C9.62555 16.5478 9.98924 17.1084 10.452 17.5814C10.9148 18.0543 11.4674 18.4301 12.0774 18.6867C12.6873 18.9432 13.3424 19.0753 14.0041 19.0753C14.6658 19.0753 15.3208 18.9432 15.9308 18.6867C16.5407 18.4301 17.0933 18.0543 17.5561 17.5814C18.0189 17.1084 18.3826 16.5478 18.6259 15.9324C18.8691 15.317 18.987 14.6592 18.9727 13.9977C18.9446 12.6985 18.4087 11.462 17.48 10.5532C16.5512 9.6443 15.3035 9.13535 14.0041 9.13535C12.7047 9.13535 11.4569 9.6443 10.5282 10.5532C9.59946 11.462 9.06361 12.6985 9.03546 13.9977ZM23.7243 14.8295C23.6151 14.8295 23.5069 14.808 23.406 14.7662C23.3051 14.7244 23.2134 14.6631 23.1362 14.5859C23.0589 14.5086 22.9977 14.4169 22.9559 14.316C22.9141 14.2151 22.8925 14.1069 22.8925 13.9977C22.8925 13.8884 22.9141 13.7803 22.9559 13.6793C22.9977 13.5784 23.0589 13.4867 23.1362 13.4095C23.2134 13.3322 23.3051 13.271 23.406 13.2292C23.5069 13.1873 23.6151 13.1658 23.7243 13.1658H27.1682C27.2774 13.1658 27.3856 13.1873 27.4865 13.2292C27.5874 13.271 27.6791 13.3322 27.7564 13.4095C27.8336 13.4867 27.8949 13.5784 27.9367 13.6793C27.9785 13.7803 28 13.8884 28 13.9977C28 14.1069 27.9785 14.2151 27.9367 14.316C27.8949 14.4169 27.8336 14.5086 27.7564 14.5859C27.6791 14.6631 27.5874 14.7244 27.4865 14.7662C27.3856 14.808 27.2774 14.8295 27.1682 14.8295H23.7243ZM0.831799 14.8295C0.722565 14.8295 0.614402 14.808 0.513483 14.7662C0.412565 14.7244 0.320868 14.6631 0.243628 14.5859C0.166389 14.5086 0.105119 14.4169 0.0633169 14.316C0.0215151 14.2151 0 14.1069 0 13.9977C0 13.8884 0.0215151 13.7803 0.0633169 13.6793C0.105119 13.5784 0.166389 13.4867 0.243628 13.4095C0.320868 13.3322 0.412565 13.271 0.513483 13.2292C0.614402 13.1873 0.722565 13.1658 0.831799 13.1658H4.27682C4.38606 13.1658 4.49422 13.1873 4.59514 13.2292C4.69606 13.271 4.78775 13.3322 4.86499 13.4095C4.94223 13.4867 5.0035 13.5784 5.0453 13.6793C5.08711 13.7803 5.10862 13.8884 5.10862 13.9977C5.10862 14.1069 5.08711 14.2151 5.0453 14.316C5.0035 14.4169 4.94223 14.5086 4.86499 14.5859C4.78775 14.6631 4.69606 14.7244 4.59514 14.7662C4.49422 14.808 4.38606 14.8295 4.27682 14.8295H0.831799ZM20.2875 7.70933C20.2099 7.63233 20.1483 7.54073 20.1063 7.43981C20.0642 7.3389 20.0426 7.23066 20.0426 7.12133C20.0426 7.01201 20.0642 6.90377 20.1063 6.80285C20.1483 6.70193 20.2099 6.61033 20.2875 6.53333L22.7222 4.0985C22.7987 4.01799 22.8905 3.9536 22.9922 3.90913C23.094 3.86467 23.2036 3.84102 23.3146 3.83959C23.4257 3.83816 23.5358 3.85898 23.6387 3.90081C23.7416 3.94264 23.835 4.00464 23.9135 4.08316C23.992 4.16168 24.054 4.25512 24.0959 4.35798C24.1377 4.46084 24.1585 4.57103 24.1571 4.68207C24.1556 4.7931 24.132 4.90272 24.0875 5.00447C24.0431 5.10621 23.9787 5.19803 23.8982 5.2745L21.4634 7.70933C21.3864 7.78694 21.2948 7.84853 21.1939 7.89056C21.093 7.9326 20.9848 7.95424 20.8755 7.95424C20.7661 7.95424 20.6579 7.9326 20.557 7.89056C20.4561 7.84853 20.3645 7.78694 20.2875 7.70933ZM6.5354 7.70933L4.103 5.27683C4.02249 5.20036 3.9581 5.10855 3.91364 5.0068C3.86917 4.90505 3.84553 4.79543 3.8441 4.6844C3.84267 4.57337 3.86348 4.46317 3.90531 4.36031C3.94714 4.25745 4.00914 4.16401 4.08766 4.08549C4.16617 4.00698 4.25961 3.94497 4.36247 3.90314C4.46532 3.86131 4.57551 3.8405 4.68654 3.84192C4.79756 3.84335 4.90718 3.867 5.00892 3.91147C5.11067 3.95593 5.20248 4.02032 5.27895 4.10083L7.71368 6.53567C7.79419 6.61214 7.85857 6.70395 7.90304 6.8057C7.9475 6.90745 7.97115 7.01707 7.97258 7.1281C7.97401 7.23913 7.95319 7.34933 7.91136 7.45219C7.86953 7.55505 7.80753 7.64849 7.72902 7.72701C7.6505 7.80552 7.55706 7.86753 7.45421 7.90936C7.35135 7.95119 7.24116 7.972 7.13014 7.97057C7.01911 7.96915 6.90949 7.9455 6.80775 7.90103C6.70601 7.85657 6.61419 7.79218 6.53773 7.71167L6.5354 7.70933ZM13.1676 4.27467V0.831833C13.1676 0.611217 13.2553 0.399637 13.4112 0.243638C13.5672 0.0876393 13.7788 0 13.9994 0C14.22 0 14.4316 0.0876393 14.5876 0.243638C14.7436 0.399637 14.8312 0.611217 14.8312 0.831833V4.27583C14.8312 4.49645 14.7436 4.70803 14.5876 4.86403C14.4316 5.02003 14.22 5.10767 13.9994 5.10767C13.7788 5.10767 13.5672 5.02003 13.4112 4.86403C13.2553 4.70803 13.1676 4.49528 13.1676 4.27467Z" fill="black"/>
</svg>
                </span>
              </div>
              <div className="col-md-3 col">
                <form class="d-flex">
                <input class="form-control " type="search"  aria-label="Search" style={{borderTopRightRadius: "0px", borderBottomRightRadius: "0px", borderTopLeftRadius: "7px", borderBottomLeftRadius: "7px"}} />
                <button class="btn btn-custom " type="submit"  style={{borderTopRightRadius: "7px", borderBottomRightRadius: "7px", borderTopLeftRadius: "0px", borderBottomLeftRadius: "0px"}}>Search</button>
              </form>
              </div>
            </div>
            <div className="table-responsive">
            <table class="table table-striped table-xl bg-white">
              <thead>
                <tr>
                  <th scope="col" className='dm-600'>S/N</th>
                  <th scope="col" className='dm-600'>Pass ID</th>
                  <th scope="col" className='dm-600'>Full Name</th>
                  <th scope="col" className='dm-600'>Phone No</th>
                  <th scope="col" className='dm-600'>CID</th>
                  <th scope="col" className='dm-600'>Host</th>
                  <th scope="col" className='dm-600'>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>031</td>
                  <td>Kelden Norbu</td>
                  <td>17330827</td>
                  <td>10906002173</td>
                  <td>Sonam Wangmo</td>
                  <td><button className="btn btn-custom">Check In</button></td>
                </tr>
                <tr className='text-muted'>
                  <th scope="row">2</th>
                  <td>031</td>
                  <td>Kelden Norbu</td>
                  <td>17330827</td>
                  <td>10906002173</td>
                  <td>Sonam Wangmo</td>
                  <td><button className="btn btn-custom">Check In</button></td>
                </tr>
                <tr className='text-muted'>
                  <th scope="row">3</th>
                  <td>031</td>
                  <td>Kelden Norbu</td>
                  <td>17330827</td>
                  <td>10906002173</td>
                  <td>Sonam Wangmo</td>
                  <td><button className="btn btn-custom">Check In</button></td>
                </tr>
                <tr className='text-muted'>
                  <th scope="row">4</th>
                  <td>031</td>
                  <td>Kelden Norbu</td>
                  <td>17330827</td>
                  <td>10906002173</td>
                  <td>Sonam Wangmo</td>
                  <td><button className="btn btn-custom">Check In</button></td>
                </tr>
              </tbody>
            </table>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    
  )
}

export default Gkdashboard;