import React, { useState,useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
const Login = ({ setIsAuthenticated, setUserId , setUserRole }) => {
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };
  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch('http://127.0.0.1:8000/api/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });

      if (response.ok) {
        // Store the access token in local storage
        const data = await response.json()
      
        localStorage.setItem('access_token', JSON.stringify(data['access_token']));
        localStorage.setItem('role', JSON.stringify(data['role']))
        localStorage.setItem('user_id', JSON.stringify(data['user_id']))

        console.log(!!localStorage.getItem("access_token"))
      // Redirect to the "Gkdashboard" component
        setIsAuthenticated(!!localStorage.getItem("access_token"))
        setUserId(localStorage.getItem("user_id"))
        setUserRole(localStorage.getItem('role'))
      } else {
        // Failed login, you can display an error message to the user
        console.error('Login failed');
        console.log(response)
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="container">
      <div className="row justify-content-center align-items-center vh-100">
        <div className="col-12 col-md-6">
          <h2 className="text-center text-dark dm-800 mb-4">Sign In</h2>
          <h6 className="dm-600 text-center mb-5 text-muted">Enter your email and password</h6>
          <div className="bg-white p-5 m-5 rounded shadow" style={{ boxShadow: '0px 0px 10px 0px rgba(0,0,0,0.1)' }}>
            <form className="my-3" onSubmit={handleLogin}>
              <div className="input-group mb-4">
                <span className="input-group-text bg-white" id="basic-addon1">
                  <svg width="16" height="19" viewBox="0 0 16 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M3 5C3 2.23858 5.23858 0 8 0C10.7614 0 13 2.23858 13 5C13 7.76142 10.7614 10 8 10C5.23858 10 3 7.76142 3 5ZM8 8C9.65685 8 11 6.65685 11 5C11 3.34315 9.65685 2 8 2C6.34315 2 5 3.34315 5 5C5 6.65685 6.34315 8 8 8Z" fill="#2E3A59"/>
                  <path d="M2.34315 13.3431C0.842855 14.8434 0 16.8783 0 19H2C2 17.4087 2.63214 15.8826 3.75736 14.7574C4.88258 13.6321 6.4087 13 8 13C9.5913 13 11.1174 13.6321 12.2426 14.7574C13.3679 15.8826 14 17.4087 14 19H16C16 16.8783 15.1571 14.8434 13.6569 13.3431C12.1566 11.8429 10.1217 11 8 11C5.87827 11 3.84344 11.8429 2.34315 13.3431Z" fill="#2E3A59"/>
                  </svg>
                </span>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Email"
                  aria-label="Email"
                  aria-describedby="basic-addon1"
                  name="email"
                  value={formData.email}
                  onChange={handleChange}
                />
              </div>
              <div className="input-group mb-4">
                <span className="input-group-text bg-white" id="basic-addon2">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="19" viewBox="0 0 20 16" fill="none">
                  <path d="M18 15.9999H2C0.930516 16.0319 0.0364164 15.1933 0 14.1239V1.87494C0.0364132 0.805748 0.930683 -0.0326298 2 -5.87853e-05H18C19.0693 -0.0326298 19.9636 0.805748 20 1.87494V14.1249C19.963 15.1939 19.0691 16.032 18 15.9999ZM2 1.99994V13.9889L18 13.9999V2.01094L2 1.99994ZM11.43 11.9999H4C4.07353 11.172 4.46534 10.4049 5.093 9.85994C5.79183 9.16666 6.73081 8.76915 7.715 8.74994C8.69919 8.76915 9.63817 9.16666 10.337 9.85994C10.9645 10.405 11.3563 11.1721 11.43 11.9999ZM16 10.9999H13V8.99994H16V10.9999ZM7.715 7.99994C7.17907 8.01856 6.65947 7.81384 6.28029 7.43465C5.9011 7.05547 5.69638 6.53587 5.715 5.99994C5.69668 5.4641 5.9015 4.94468 6.28062 4.56556C6.65974 4.18644 7.17916 3.98162 7.715 3.99994C8.25084 3.98162 8.77026 4.18644 9.14938 4.56556C9.5285 4.94468 9.73332 5.4641 9.715 5.99994C9.73362 6.53587 9.5289 7.05547 9.14971 7.43465C8.77053 7.81384 8.25093 8.01856 7.715 7.99994ZM16 6.99994H12V4.99994H16V6.99994Z" fill="#2E3A59"/>
                  </svg>
                </span>
                <input
                  type="password"
                  className="form-control"
                  placeholder="Password"
                  aria-label="Password"
                  aria-describedby="basic-addon2"
                  name="password"
                  value={formData.password}
                  onChange={handleChange}
                />
              </div>
              <div className="mb-4 form-check">
                <input type="checkbox" className="form-check-input" id="rememberMe" />
                <label className="form-check-label" htmlFor="rememberMe">Remember my password</label>
              </div>
              <button type="submit" className="btn w-100 p-2" style={{ backgroundColor: "#4f4f4f" }}>
                <span className="dm-600 text-white">Login</span>
              </button>
            </form>
            <div className="text-center mt-4">
              <a href="#" className="text-muted">Forgot Password?</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
