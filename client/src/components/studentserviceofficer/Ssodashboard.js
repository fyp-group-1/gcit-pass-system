import React from 'react'
import Sidebar from './sidebar/Sidebar';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './navbar.css'
function Ssodashboard({ setIsAuthenticated }) {
  // const handleLogout = () => {
  //   // Clear the access token from local storage
  //   localStorage.removeItem('access_token');
  //   setIsAuthenticated(localStorage.removeItem('access_token'))
  // };

  return (
    <div class="row vh-100 vw-100">
      {console.log(localStorage.getItem('access_token'))}
      <div class="col-3">
        <Sidebar setIsAuthenticated={setIsAuthenticated} ></Sidebar>
      </div>
      
      {/* main container */}
      <div class="col-9 container vh-100 d-flex flex-column ">
        <nav class="navbar navbar-expand-lg navbar-light bg-white p-4 row">
          <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          <div class="collapse navbar-collapse bg-" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 pe-5">
              <li class="nav-item">

                <button type="button" class="btn btn-custom btn-lg mx-auto" hidden>Large button</button>
              </li>
            </ul>

            <ul className="noti-1">
                <svg
                  width={24}
                  height={24}
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M18 8C18 6.4087 17.3679 4.88258 16.2426 3.75736C15.1174 2.63214 13.5913 2 12 2C10.4087 2 8.88258 2.63214 7.75736 3.75736C6.63214 4.88258 6 6.4087 6 8C6 15 3 17 3 17H21C21 17 18 15 18 8Z"
                    fill="#E3E3E3"
                  />
                  <path
                    d="M13.7295 21C13.5537 21.3031 13.3014 21.5547 12.9978 21.7295C12.6941 21.9044 12.3499 21.9965 11.9995 21.9965C11.6492 21.9965 11.3049 21.9044 11.0013 21.7295C10.6977 21.5547 10.4453 21.3031 10.2695 21"
                    stroke="#E3E3E3"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <circle
                    cx="17.7024"
                    cy="4.69852"
                    r="3.97196"
                    fill="#BB4430"
                  />
                  <path
                    d="M18 8C18 6.4087 17.3679 4.88258 16.2426 3.75736C15.1174 2.63214 13.5913 2 12 2C10.4087 2 8.88258 2.63214 7.75736 3.75736C6.63214 4.88258 6 6.4087 6 8C6 15 3 17 3 17H21C21 17 18 15 18 8Z"
                    fill="#E3E3E3"
                  />
                  <path
                    d="M13.7295 21C13.5537 21.3031 13.3014 21.5547 12.9978 21.7295C12.6941 21.9044 12.3499 21.9965 11.9995 21.9965C11.6492 21.9965 11.3049 21.9044 11.0013 21.7295C10.6977 21.5547 10.4453 21.3031 10.2695 21"
                    stroke="#E3E3E3"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <circle
                    cx="17.7024"
                    cy="4.69852"
                    r="3.97196"
                    fill="#BB4430"
                  />
                </svg>
              </ul>



          
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link m-0 p-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <div className="row p-0 m-0 d-flex flex-nowrap justify-content-center align-items-center w-100 ">
                  <div className="col-4 m-0 p-0 d-flex justify-content-center align-items-center" style={{width: "45px"}}>
                      <svg
              width={38}
              height={38}
              viewBox="0 0 36 36"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <circle cx="17.5292" cy="17.5292" r="17.5292" fill="#8DC63F" />
              <mask
                id="mask0_222_7594"
                style={{ maskType: "alpha" }}
                maskUnits="userSpaceOnUse"
                x={0}
                y={0}
                width={36}
                height={36}
              >
                <circle cx="17.7011" cy="17.5292" r="17.5292" fill="#FFC145" />
              </mask>
              <g mask="url(#mask0_222_7594)">
                <mask
                  id="mask1_222_7594"
                  style={{ maskType: "alpha" }}
                  maskUnits="userSpaceOnUse"
                  x={0}
                  y={0}
                  width={36}
                  height={36}
                >
                  <circle cx="17.7011" cy="17.5292" r="17.5292" fill="#FFC145" />
                </mask>
                <g mask="url(#mask1_222_7594)">
                  <path
                    d="M28.5328 37.1468V33.566C28.5328 31.6666 27.9622 29.845 26.9464 28.5019C25.9306 27.1588 24.5529 26.4043 23.1164 26.4043H12.2836C10.8471 26.4043 9.46939 27.1588 8.45362 28.5019C7.43784 29.845 6.86719 31.6666 6.86719 33.566V37.1468"
                    fill="#4F4F4F"
                  />
                  <path
                    d="M17.6996 20.9871C20.691 20.9871 23.116 18.5621 23.116 15.5707C23.116 12.5793 20.691 10.1543 17.6996 10.1543C14.7082 10.1543 12.2832 12.5793 12.2832 15.5707C12.2832 18.5621 14.7082 20.9871 17.6996 20.9871Z"
                    fill="white"
                  />
                </g>
              </g>
                      </svg>
                  </div>
                  <div className="col-8 m-0 p-0 ms-1 h-100 flex-grow-1 d-flex flex-column justify-content-center align-items-start" style={{height: '50px'}}>
                      <div className=' m-0 p-0 dm-600 text-nowrap text-dark' style={{fontSize: "14px"}}>Sangay Lhamo</div>
                      <div className=' m-0 p-0 dm-500 '  style={{fontSize: "12px", color: '#AEAEAE'}}>Student Service Officer</div>
                  </div>
                </div>
              </a>
              <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#">Action</a></li>
                <li><a class="dropdown-item" href="#">Another action</a></li>
                <li><hr class="dropdown-divider" /></li>
                <li><a class ="dropdown-item" href="#">Something else here</a></li>
              </ul>
            </li>
          </ul>
        </div>
          </div>
        </nav> 
        {/* MAIN CONTAINER */}
        <div className="container scrollable-div  w-100  py-5" style={{overflow: "auto", height: "825px"}}>
          <div class="row row-cols-1 row-cols-md-3 g-4">
            <div class="col-lg-3 col-md-4 col-sm-6 col-12 mx-auto">
              <div class=" h-100 w-100">
              <div className='col-lg-3 col-md-4 col-sm-6 col-12 mx-auto w-100 h-100 d-flex justify-content-center align-items-center'>
                <div className='shadow-lg d-flex flex-column align-items-center w' style={{ width: '250px', height: '300px', margin: '10px', borderRadius: "25px", }}>
                  <h1 style={{ fontSize: "85px", color: "#828282",paddingTop:"35px"}}>3</h1>
                  <div className='row' style={{ margin: '0', padding: '0' }}>
                  <div className='col'>
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="19" viewBox="0 0 20 19" fill="none">
                    <path d="M20 19H0V9C0 7.89543 0.89543 7 2 7H6V2C6 0.89543 6.89543 0 8 0H12C13.1046 0 14 0.89543 14 2V5H18C19.1046 5 20 5.89543 20 7V19ZM14 7V17H18V7H14ZM8 2V17H12V2H8ZM2 9V17H6V9H2Z" fill="#BDBDBD" />
                  </svg>
                  </div>
                  <div className='col' style={{ margin: '0', padding: '0' }}>
                  <p style={{ color: "#cfcfcf", paddingTop:"4px" }} className='dm-600'>Total</p>
                  </div>
                  </div>
                  <div className="second-svg" style={{paddingTop:"23px" }}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="23" viewBox="0 0 20 18" fill="none">
                      <path d="M7 0C4.23858 0 2 2.23858 2 5C2 7.76142 4.23858 10 7 10C9.76142 10 12 7.76142 12 5C12 2.23858 9.76142 0 7 0ZM4 5C4 3.34315 5.34315 2 7 2C8.65685 2 10 3.34315 10 5C10 6.65685 8.65685 8 7 8C5.34315 8 4 6.65685 4 5Z" fill="#2E3A59" />
                      <path d="M14.9084 5.21828C14.6271 5.07484 14.3158 5.00006 14 5.00006V3.00006C14.6316 3.00006 15.2542 3.14956 15.8169 3.43645C15.8789 3.46805 15.9399 3.50121 16 3.5359C16.4854 3.81614 16.9072 4.19569 17.2373 4.65055C17.6083 5.16172 17.8529 5.75347 17.9512 6.37737C18.0496 7.00126 17.9987 7.63958 17.8029 8.24005C17.6071 8.84053 17.2719 9.38611 16.8247 9.83213C16.3775 10.2782 15.8311 10.6119 15.2301 10.8062C14.6953 10.979 14.1308 11.037 13.5735 10.9772C13.5046 10.9698 13.4357 10.9606 13.367 10.9496C12.7438 10.8497 12.1531 10.6038 11.6431 10.2319L11.6421 10.2311L12.821 8.61557C13.0761 8.80172 13.3717 8.92477 13.6835 8.97474C13.9953 9.02471 14.3145 9.00014 14.615 8.90302C14.9155 8.80591 15.1887 8.63903 15.4123 8.41602C15.6359 8.19302 15.8035 7.92024 15.9014 7.62001C15.9993 7.31978 16.0247 7.00063 15.9756 6.68869C15.9264 6.37675 15.8041 6.08089 15.6186 5.82531C15.4331 5.56974 15.1898 5.36172 14.9084 5.21828Z" fill="#2E3A59" />
                      <path d="M17.9981 18C17.9981 17.475 17.8947 16.9551 17.6938 16.47C17.4928 15.9849 17.1983 15.5442 16.8271 15.1729C16.4558 14.8017 16.0151 14.5072 15.53 14.3062C15.0449 14.1053 14.525 14.0019 14 14.0019V12C14.6821 12 15.3584 12.1163 16 12.3431C16.0996 12.3783 16.1983 12.4162 16.2961 12.4567C17.0241 12.7583 17.6855 13.2002 18.2426 13.7574C18.7998 14.3145 19.2417 14.9759 19.5433 15.7039C19.5838 15.8017 19.6217 15.9004 19.6569 16C19.8837 16.6416 20 17.3179 20 18H17.9981Z" fill="#2E3A59" />
                      <path d="M14 18H12C12 15.2386 9.76142 13 7 13C4.23858 13 2 15.2386 2 18H0C0 14.134 3.13401 11 7 11C10.866 11 14 14.134 14 18Z" fill="#2E3A59" />
                    </svg>
                  </div>
                  <h1 style={{ fontSize: "22px", fontFamily: 'DM Sans, sans-serif',paddingTop:"10px" }} className='dm-500'>Visitor Expected</h1>
                </div>
              </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-12 mx-auto">
              <div class=" h-100 w-100">
              <div className='col-lg-3 col-md-4 col-sm-6 col-12 mx-auto w-100 h-100 d-flex justify-content-center align-items-center'>
                <div className='shadow-lg d-flex flex-column align-items-center w' style={{ width: '250px', height: '300px', margin: '10px', borderRadius: "25px", }}>
                <h1 style={{ fontSize: "85px", color: "#828282" ,paddingTop:"35px" }} className='dm-500'>4</h1>
                  <div className='row' style={{ margin: '0', padding: '0' }}>
                  <div className='col'>
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="19" viewBox="0 0 20 19" fill="none">
                    <path d="M20 19H0V9C0 7.89543 0.89543 7 2 7H6V2C6 0.89543 6.89543 0 8 0H12C13.1046 0 14 0.89543 14 2V5H18C19.1046 5 20 5.89543 20 7V19ZM14 7V17H18V7H14ZM8 2V17H12V2H8ZM2 9V17H6V9H2Z" fill="#BDBDBD" />
                  </svg>
                  </div>
                  <div className='col' style={{ margin: '0', padding: '0' }}>
                  <p style={{ color: "#cfcfcf", paddingTop:"4px"}} className='dm-600'>Total</p>
                  </div>
                  </div>
                  <div className="second-svg" style={{paddingTop:"23px"}}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="23" viewBox="0 0 20 18" fill="none">
                      <path d="M7 0C4.23858 0 2 2.23858 2 5C2 7.76142 4.23858 10 7 10C9.76142 10 12 7.76142 12 5C12 2.23858 9.76142 0 7 0ZM4 5C4 3.34315 5.34315 2 7 2C8.65685 2 10 3.34315 10 5C10 6.65685 8.65685 8 7 8C5.34315 8 4 6.65685 4 5Z" fill="#2E3A59" />
                      <path d="M14.9084 5.21828C14.6271 5.07484 14.3158 5.00006 14 5.00006V3.00006C14.6316 3.00006 15.2542 3.14956 15.8169 3.43645C15.8789 3.46805 15.9399 3.50121 16 3.5359C16.4854 3.81614 16.9072 4.19569 17.2373 4.65055C17.6083 5.16172 17.8529 5.75347 17.9512 6.37737C18.0496 7.00126 17.9987 7.63958 17.8029 8.24005C17.6071 8.84053 17.2719 9.38611 16.8247 9.83213C16.3775 10.2782 15.8311 10.6119 15.2301 10.8062C14.6953 10.979 14.1308 11.037 13.5735 10.9772C13.5046 10.9698 13.4357 10.9606 13.367 10.9496C12.7438 10.8497 12.1531 10.6038 11.6431 10.2319L11.6421 10.2311L12.821 8.61557C13.0761 8.80172 13.3717 8.92477 13.6835 8.97474C13.9953 9.02471 14.3145 9.00014 14.615 8.90302C14.9155 8.80591 15.1887 8.63903 15.4123 8.41602C15.6359 8.19302 15.8035 7.92024 15.9014 7.62001C15.9993 7.31978 16.0247 7.00063 15.9756 6.68869C15.9264 6.37675 15.8041 6.08089 15.6186 5.82531C15.4331 5.56974 15.1898 5.36172 14.9084 5.21828Z" fill="#2E3A59" />
                      <path d="M17.9981 18C17.9981 17.475 17.8947 16.9551 17.6938 16.47C17.4928 15.9849 17.1983 15.5442 16.8271 15.1729C16.4558 14.8017 16.0151 14.5072 15.53 14.3062C15.0449 14.1053 14.525 14.0019 14 14.0019V12C14.6821 12 15.3584 12.1163 16 12.3431C16.0996 12.3783 16.1983 12.4162 16.2961 12.4567C17.0241 12.7583 17.6855 13.2002 18.2426 13.7574C18.7998 14.3145 19.2417 14.9759 19.5433 15.7039C19.5838 15.8017 19.6217 15.9004 19.6569 16C19.8837 16.6416 20 17.3179 20 18H17.9981Z" fill="#2E3A59" />
                      <path d="M14 18H12C12 15.2386 9.76142 13 7 13C4.23858 13 2 15.2386 2 18H0C0 14.134 3.13401 11 7 11C10.866 11 14 14.134 14 18Z" fill="#2E3A59" />
                    </svg>
                  </div>
                  <h1 style={{ fontSize: "22px", fontFamily: 'DM Sans, sans-serif',paddingTop:"10px" }}>Off-campus count</h1>
                </div>
              </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-12 mx-auto">
              <div class=" h-100 w-100">
              <div className='col-lg-3 col-md-4 col-sm-6 col-12 mx-auto w-100 h-100 d-flex justify-content-center align-items-center'>
                <div className='shadow-lg d-flex flex-column align-items-center w' style={{ width: '250px', height: '300px', margin: '10px', borderRadius: "25px", }}>
                <h1 style={{ fontSize: "85px", color: "#828282" ,paddingTop:"35px"}}>378</h1>
                  <div className='row' style={{ margin: '0', padding: '0' }}>
                  <div className='col'>
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="19" viewBox="0 0 20 19" fill="none">
                    <path d="M20 19H0V9C0 7.89543 0.89543 7 2 7H6V2C6 0.89543 6.89543 0 8 0H12C13.1046 0 14 0.89543 14 2V5H18C19.1046 5 20 5.89543 20 7V19ZM14 7V17H18V7H14ZM8 2V17H12V2H8ZM2 9V17H6V9H2Z" fill="#BDBDBD" />
                  </svg>
                  </div>
                  <div className='col' style={{ margin: '0', padding: '0' }}>
                  <p style={{ color: "#cfcfcf", paddingTop:"4px" }} className='dm-600'>Total</p>
                  </div>
                  </div>
                  <div className="second-svg" style={{paddingTop:"23px" }}>
                  <svg xmlns="http://www.w3.org/2000/svg" width="35" height="23" viewBox="0 0 20 15" fill="none">
                  <path d="M2 15H0C0 11.6863 2.68629 9 6 9C9.31371 9 12 11.6863 12 15H10C10 12.7909 8.20914 11 6 11C3.79086 11 2 12.7909 2 15ZM18.294 11.706L16 9.413L13.707 11.706L12.293 10.292L14.585 8L12.293 5.707L13.707 4.293L16 6.586L18.293 4.293L19.707 5.707L17.414 8L19.707 10.293L18.294 11.706ZM6 8C3.79086 8 2 6.20914 2 4C2 1.79086 3.79086 0 6 0C8.20914 0 10 1.79086 10 4C9.99724 6.208 8.208 7.99725 6 8ZM6 2C4.9074 2.00111 4.01789 2.87885 4.00223 3.97134C3.98658 5.06383 4.85057 5.9667 5.94269 5.99912C7.03481 6.03153 7.95083 5.1815 8 4.09V4.49V4C8 2.89543 7.10457 2 6 2Z" fill="#2E3A59"/>
                  </svg>
                    </div>
                  <h1 style={{ fontSize: "22px", fontFamily: 'DM Sans, sans-serif',paddingTop:"7px" }}>In-campus count</h1>
                </div>
              </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-12 mx-auto">
              <div class=" h-100 w-100">
              <div className='col-lg-3 col-md-4 col-sm-6 col-12 mx-auto w-100 h-100 d-flex justify-content-center align-items-center'>
                <div className='shadow-lg d-flex flex-column align-items-center w' style={{ width: '250px', height: '300px', margin: '10px', borderRadius: "25px", }}>
                <h1 style={{ fontSize: "85px", color: "#828282" ,paddingTop:"35px"}}>3</h1>
                  <div className='row' style={{ margin: '0', padding: '0' }}>
                  <div className='col'>
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="19" viewBox="0 0 20 19" fill="none">
                    <path d="M20 19H0V9C0 7.89543 0.89543 7 2 7H6V2C6 0.89543 6.89543 0 8 0H12C13.1046 0 14 0.89543 14 2V5H18C19.1046 5 20 5.89543 20 7V19ZM14 7V17H18V7H14ZM8 2V17H12V2H8ZM2 9V17H6V9H2Z" fill="#BDBDBD" />
                  </svg>
                  </div>
                  <div className='col' style={{ margin: '0', padding: '0' }}>
                  <p style={{ color: "#cfcfcf", paddingTop:"4px" }} className='dm-600'>Total</p>
                  </div>
                  </div>
                  <div className="second-svg" style={{paddingTop:"25px"}}>
                  <svg xmlns="http://www.w3.org/2000/svg" width="35" height="23" viewBox="0 0 19 17" fill="none">
                  <path d="M2 17.0012H0C0 13.6875 2.68629 11.0012 6 11.0012C9.31371 11.0012 12 13.6875 12 17.0012H10C10 14.7921 8.20914 13.0012 6 13.0012C3.79086 13.0012 2 14.7921 2 17.0012ZM16.364 13.3642L14.95 11.9502C16.2629 10.6375 17.0005 8.85687 17.0005 7.00023C17.0005 5.14359 16.2629 3.36301 14.95 2.05023L16.364 0.63623C19.8781 4.15084 19.8781 9.84862 16.364 13.3632V13.3642ZM13.535 10.5362L12.121 9.12023C13.2908 7.94896 13.2908 6.0515 12.121 4.88023L13.535 3.46323C15.4876 5.41584 15.4876 8.58162 13.535 10.5342V10.5362ZM6 10.0002C3.79086 10.0002 2 8.20937 2 6.00023C2 3.79109 3.79086 2.00023 6 2.00023C8.20914 2.00023 10 3.79109 10 6.00023C10 7.0611 9.57857 8.07851 8.82843 8.82866C8.07828 9.5788 7.06087 10.0002 6 10.0002ZM6 4.00023C4.9074 4.00134 4.01789 4.87908 4.00223 5.97157C3.98658 7.06406 4.85057 7.96693 5.94269 7.99935C7.03481 8.03176 7.95083 7.18173 8 6.09023V6.49023V6.00023C8 4.89566 7.10457 4.00023 6 4.00023Z" fill="#2E3A59"/>
                  </svg>
                  </div>
                  <h1 style={{ fontSize: "22px", fontFamily: 'DM Sans, sans-serif',paddingTop:"7px" }}>Defaulted Visitor </h1>
                </div>
              </div>
              </div>
            </div>
          </div>
        {/* BOTTOM CONTAINTER */}

            

          
        </div>
      </div>
    </div>
    
  )
}

export default Ssodashboard;