import React from 'react'
import Sidebar from './sidebar/Sidebar';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './navbar.css'
function ViewStudentLog({ setIsAuthenticated }) {
  // const handleLogout = () => {
  //   // Clear the access token from local storage
  //   localStorage.removeItem('access_token');
  //   setIsAuthenticated(localStorage.removeItem('access_token'))
  // };

  return (
    <div class="row vh-100 vw-100">
      {console.log(localStorage.getItem('access_token'))}
      <div class="col-3">
        <Sidebar setIsAuthenticated={setIsAuthenticated} ></Sidebar>
      </div>
      {/* main container */}
      <div class="col-9 container vh-100 d-flex flex-column ">
        <nav class="navbar navbar-expand-lg navbar-light bg-white p-4 row">
          <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          <div class="collapse navbar-collapse bg-" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 pe-5">
              <li class="nav-item">

                <button type="button" class="btn btn-custom btn-lg mx-auto" hidden>Large button</button>
              </li>
            </ul>

            <ul className="noti-1">
                <svg
                  width={24}
                  height={24}
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M18 8C18 6.4087 17.3679 4.88258 16.2426 3.75736C15.1174 2.63214 13.5913 2 12 2C10.4087 2 8.88258 2.63214 7.75736 3.75736C6.63214 4.88258 6 6.4087 6 8C6 15 3 17 3 17H21C21 17 18 15 18 8Z"
                    fill="#E3E3E3"
                  />
                  <path
                    d="M13.7295 21C13.5537 21.3031 13.3014 21.5547 12.9978 21.7295C12.6941 21.9044 12.3499 21.9965 11.9995 21.9965C11.6492 21.9965 11.3049 21.9044 11.0013 21.7295C10.6977 21.5547 10.4453 21.3031 10.2695 21"
                    stroke="#E3E3E3"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <circle
                    cx="17.7024"
                    cy="4.69852"
                    r="3.97196"
                    fill="#BB4430"
                  />
                  <path
                    d="M18 8C18 6.4087 17.3679 4.88258 16.2426 3.75736C15.1174 2.63214 13.5913 2 12 2C10.4087 2 8.88258 2.63214 7.75736 3.75736C6.63214 4.88258 6 6.4087 6 8C6 15 3 17 3 17H21C21 17 18 15 18 8Z"
                    fill="#E3E3E3"
                  />
                  <path
                    d="M13.7295 21C13.5537 21.3031 13.3014 21.5547 12.9978 21.7295C12.6941 21.9044 12.3499 21.9965 11.9995 21.9965C11.6492 21.9965 11.3049 21.9044 11.0013 21.7295C10.6977 21.5547 10.4453 21.3031 10.2695 21"
                    stroke="#E3E3E3"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <circle
                    cx="17.7024"
                    cy="4.69852"
                    r="3.97196"
                    fill="#BB4430"
                  />
                </svg>
              </ul>



          
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link m-0 p-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <div className="row p-0 m-0 d-flex flex-nowrap justify-content-center align-items-center w-100 ">
                  <div className="col-4 m-0 p-0 d-flex justify-content-center align-items-center" style={{width: "45px"}}>
                      <svg
              width={38}
              height={38}
              viewBox="0 0 36 36"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <circle cx="17.5292" cy="17.5292" r="17.5292" fill="#8DC63F" />
              <mask
                id="mask0_222_7594"
                style={{ maskType: "alpha" }}
                maskUnits="userSpaceOnUse"
                x={0}
                y={0}
                width={36}
                height={36}
              >
                <circle cx="17.7011" cy="17.5292" r="17.5292" fill="#FFC145" />
              </mask>
              <g mask="url(#mask0_222_7594)">
                <mask
                  id="mask1_222_7594"
                  style={{ maskType: "alpha" }}
                  maskUnits="userSpaceOnUse"
                  x={0}
                  y={0}
                  width={36}
                  height={36}
                >
                  <circle cx="17.7011" cy="17.5292" r="17.5292" fill="#FFC145" />
                </mask>
                <g mask="url(#mask1_222_7594)">
                  <path
                    d="M28.5328 37.1468V33.566C28.5328 31.6666 27.9622 29.845 26.9464 28.5019C25.9306 27.1588 24.5529 26.4043 23.1164 26.4043H12.2836C10.8471 26.4043 9.46939 27.1588 8.45362 28.5019C7.43784 29.845 6.86719 31.6666 6.86719 33.566V37.1468"
                    fill="#4F4F4F"
                  />
                  <path
                    d="M17.6996 20.9871C20.691 20.9871 23.116 18.5621 23.116 15.5707C23.116 12.5793 20.691 10.1543 17.6996 10.1543C14.7082 10.1543 12.2832 12.5793 12.2832 15.5707C12.2832 18.5621 14.7082 20.9871 17.6996 20.9871Z"
                    fill="white"
                  />
                </g>
              </g>
                      </svg>
                  </div>
                  <div className="col-8 m-0 p-0 ms-1 h-100 flex-grow-1 d-flex flex-column justify-content-center align-items-start" style={{height: '50px'}}>
                      <div className=' m-0 p-0 dm-600 text-nowrap text-dark' style={{fontSize: "14px"}}>Sangay Lhamo</div>
                      <div className=' m-0 p-0 dm-500 '  style={{fontSize: "12px", color: '#AEAEAE'}}>Student Service Officer</div>
                  </div>
                </div>
              </a>
              <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#">Action</a></li>
                <li><a class="dropdown-item" href="#">Another action</a></li>
                <li><hr class="dropdown-divider" /></li>
                <li><a class ="dropdown-item" href="#">Something else here</a></li>
              </ul>
            </li>
          </ul>
        </div>
          </div>
        </nav> 
        {/* MAIN CONTAINER */}
        <div className="row d-flex justify-content-between p-5 ">
              <div className="col-md-3 col">
                <form class="d-flex">
                <input class="form-control " type="search"  aria-label="Search" style={{borderTopRightRadius: "0px", borderBottomRightRadius: "0px", borderTopLeftRadius: "7px", borderBottomLeftRadius: "7px"}} />
                <button class="btn btn-custom " type="submit"  style={{borderTopRightRadius: "7px", borderBottomRightRadius: "7px", borderTopLeftRadius: "0px", borderBottomLeftRadius: "0px"}}>Search</button>
              </form>
              </div>
              <div className="col-md-2 col d-flex align-items-center">
                <svg className='me-4' xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20" fill="none">
                <path d="M16 20H2C0.89543 20 0 19.1046 0 18V4C0 2.89543 0.89543 2 2 2H4V0H6V2H12V0H14V2H16C17.1046 2 18 2.89543 18 4V18C18 19.1046 17.1046 20 16 20ZM2 8V18H16V8H2ZM2 4V6H16V4H2ZM14 16H12V14H14V16ZM10 16H8V14H10V16ZM6 16H4V14H6V16ZM14 12H12V10H14V12ZM10 12H8V10H10V12ZM6 12H4V10H6V12Z" fill="#4F4F4F"/>
                </svg>
              <h className="dm-600 me-4">Today</h>
                <svg className='me-4' xmlns="http://www.w3.org/2000/svg" width="13" height="8" viewBox="0 0 13 8" fill="none">
                <path d="M6.00023 7.71309L12.0102 1.70309L10.5972 0.288086L6.00023 4.88809L1.40423 0.288086L-0.00976562 1.70209L6.00023 7.71309Z" fill="#4F4F4F"/>
                </svg>
              </div>
              <div className="bot-container my-5  p-5 shadow-lg rounded">
            <div className="row my-4 d-flex justify-content-between">
              <div className="col-md-3 col">
                
              <h1 className='dm-600 fs-5 '> <span className='me-2'>Staff List</span > <svg width="20" height="20" viewBox="0 0 20 20" className='me-3 text-dark'  fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M18 18H1C0.447715 18 0 17.5523 0 17V0H2V16H18V18ZM16.627 13L12.457 8.918L10.229 11.1C9.84673 11.4715 9.23827 11.4715 8.856 11.1L4 6.344L5.373 5L9.543 9.082L11.77 6.9C12.1523 6.52848 12.7607 6.52848 13.143 6.9L18 11.656L16.627 13Z" fill="#2E3A59"/>
              </svg></h1> 
              </div>
              <div className="col-md-2 col">
              <button className='btn bg-green text-white'>
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20" fill="none">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M11.586 0C12.0556 0.000114731 12.5101 0.165434 12.87 0.467L13 0.586L17.414 5C17.746 5.33202 17.9506 5.77028 17.992 6.238L18 6.414V18C18.0002 18.5046 17.8096 18.9906 17.4665 19.3605C17.1234 19.7305 16.6532 19.9572 16.15 19.995L16 20H10V18H16V8H11.5C11.1271 7.99998 10.7676 7.86108 10.4916 7.61038C10.2156 7.35968 10.0428 7.01516 10.007 6.644L10 6.5V2H4V10H2V2C1.99984 1.49542 2.19041 1.00943 2.5335 0.639452C2.87659 0.269471 3.34684 0.0428433 3.85 0.00500011L4 0H11.586ZM5.707 12.464L8.536 15.293C8.72347 15.4805 8.82879 15.7348 8.82879 16C8.82879 16.2652 8.72347 16.5195 8.536 16.707L5.707 19.535C5.61475 19.6305 5.50441 19.7067 5.3824 19.7591C5.2604 19.8115 5.12918 19.8391 4.9964 19.8403C4.86362 19.8414 4.73194 19.8161 4.60905 19.7658C4.48615 19.7155 4.3745 19.6413 4.2806 19.5474C4.18671 19.4535 4.11246 19.3419 4.06218 19.219C4.0119 19.0961 3.9866 18.9644 3.98775 18.8316C3.9889 18.6988 4.01649 18.5676 4.0689 18.4456C4.12131 18.3236 4.19749 18.2132 4.293 18.121L5.414 17H1C0.734784 17 0.48043 16.8946 0.292893 16.7071C0.105357 16.5196 0 16.2652 0 16C0 15.7348 0.105357 15.4804 0.292893 15.2929C0.48043 15.1054 0.734784 15 1 15H5.414L4.293 13.879C4.20009 13.7862 4.12638 13.6759 4.07607 13.5546C4.02576 13.4333 3.99984 13.3032 3.9998 13.1719C3.9997 12.9066 4.10499 12.6521 4.2925 12.4645C4.48001 12.2769 4.73438 12.1714 4.99965 12.1713C5.131 12.1713 5.26107 12.1971 5.38243 12.2473C5.5038 12.2975 5.61409 12.3712 5.707 12.464ZM12 2.414V6H15.586L12 2.414Z" fill="white"/>
                </svg> <span className='ms-2'> Import</span> </button>
              </div>
            </div>
            <div className="table-responsive">
            <table class="table table-striped table-xl bg-white">
              <thead>
                <tr>
                  <th scope="col" className='dm-600'>Pass ID</th>
                  <th scope="col" className='dm-600'>Name</th>
                  <th scope="col" className='dm-600'>Email</th>
                  <th scope="col" className='dm-600'>Position Title</th>
                  <th scope="col" className='dm-600'>Phone No</th>
                  <th scope="col" className='dm-600'>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr className='text-muted '>
                  <th scope="row" >1</th>
                  <td>Pema Dorji</td>
                  <td>sonamwangmo.gcit@rub.edu.bt</td>
                  <td>Lecturer</td>
                  <td>17002173</td>
                  <td>
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20" fill="none">
                    <path d="M14 20H4C2.89543 20 2 19.1046 2 18V5H0V3H4V2C4 0.89543 4.89543 0 6 0H12C13.1046 0 14 0.89543 14 2V3H18V5H16V18C16 19.1046 15.1046 20 14 20ZM4 5V18H14V5H4ZM6 2V3H12V2H6ZM12 16H10V7H12V16ZM8 16H6V7H8V16Z" fill="#2E3A59"/>
                    </svg>
                    <svg className="ms-3"xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                    <path d="M1.13107 13.5767C0.932373 13.5764 0.742955 13.4926 0.609031 13.3458C0.472638 13.2002 0.404864 13.0033 0.422739 12.8046L0.596281 10.8964L8.6132 2.8823L11.1186 5.38696L3.10378 13.4003L1.19553 13.5739C1.17357 13.576 1.15161 13.5767 1.13107 13.5767ZM11.6187 4.88617L9.11399 2.38151L10.6164 0.879131C10.7492 0.746123 10.9295 0.671387 11.1175 0.671387C11.3055 0.671387 11.4858 0.746123 11.6187 0.879131L13.121 2.38151C13.254 2.51437 13.3288 2.69465 13.3288 2.88265C13.3288 3.07065 13.254 3.25094 13.121 3.3838L11.6194 4.88546L11.6187 4.88617Z" fill="#2E3A59"/>
                    </svg>
                  </td>
                </tr>
                <tr className='text-muted'>
                  <th scope="row">2</th>
                  <td>Pema Dorji</td>
                  <td>sonamwangmo.gcit@rub.edu.bt</td>
                  <td>Associate Lecturer</td>
                  <td>17602173</td>
                  <td>
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20" fill="none">
                    <path d="M14 20H4C2.89543 20 2 19.1046 2 18V5H0V3H4V2C4 0.89543 4.89543 0 6 0H12C13.1046 0 14 0.89543 14 2V3H18V5H16V18C16 19.1046 15.1046 20 14 20ZM4 5V18H14V5H4ZM6 2V3H12V2H6ZM12 16H10V7H12V16ZM8 16H6V7H8V16Z" fill="#2E3A59"/>
                    </svg>
                    <svg className="ms-3"xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                    <path d="M1.13107 13.5767C0.932373 13.5764 0.742955 13.4926 0.609031 13.3458C0.472638 13.2002 0.404864 13.0033 0.422739 12.8046L0.596281 10.8964L8.6132 2.8823L11.1186 5.38696L3.10378 13.4003L1.19553 13.5739C1.17357 13.576 1.15161 13.5767 1.13107 13.5767ZM11.6187 4.88617L9.11399 2.38151L10.6164 0.879131C10.7492 0.746123 10.9295 0.671387 11.1175 0.671387C11.3055 0.671387 11.4858 0.746123 11.6187 0.879131L13.121 2.38151C13.254 2.51437 13.3288 2.69465 13.3288 2.88265C13.3288 3.07065 13.254 3.25094 13.121 3.3838L11.6194 4.88546L11.6187 4.88617Z" fill="#2E3A59"/>
                    </svg>
                  </td>
                </tr>
                <tr className='text-muted'>
                  <th scope="row">3</th>
                  <td>Pema Dorji</td>
                  <td>sonamwangmo.gcit@rub.edu.bt</td>
                  <td>Lecturer</td>
                  <td>17602173</td>
                  <td>
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20" fill="none">
                    <path d="M14 20H4C2.89543 20 2 19.1046 2 18V5H0V3H4V2C4 0.89543 4.89543 0 6 0H12C13.1046 0 14 0.89543 14 2V3H18V5H16V18C16 19.1046 15.1046 20 14 20ZM4 5V18H14V5H4ZM6 2V3H12V2H6ZM12 16H10V7H12V16ZM8 16H6V7H8V16Z" fill="#2E3A59"/>
                    </svg>
                    <svg className="ms-3"xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                    <path d="M1.13107 13.5767C0.932373 13.5764 0.742955 13.4926 0.609031 13.3458C0.472638 13.2002 0.404864 13.0033 0.422739 12.8046L0.596281 10.8964L8.6132 2.8823L11.1186 5.38696L3.10378 13.4003L1.19553 13.5739C1.17357 13.576 1.15161 13.5767 1.13107 13.5767ZM11.6187 4.88617L9.11399 2.38151L10.6164 0.879131C10.7492 0.746123 10.9295 0.671387 11.1175 0.671387C11.3055 0.671387 11.4858 0.746123 11.6187 0.879131L13.121 2.38151C13.254 2.51437 13.3288 2.69465 13.3288 2.88265C13.3288 3.07065 13.254 3.25094 13.121 3.3838L11.6194 4.88546L11.6187 4.88617Z" fill="#2E3A59"/>
                    </svg>
                  </td>
                </tr>
                <tr className='text-muted'>
                  <th scope="row">4</th>
                  <td>Pema Dorji</td>
                  <td>sonamwangmo.gcit@rub.edu.bt</td>
                  <td>Associate Lecturer</td>
                  <td>17602173</td>
                  <td>
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20" fill="none">
                    <path d="M14 20H4C2.89543 20 2 19.1046 2 18V5H0V3H4V2C4 0.89543 4.89543 0 6 0H12C13.1046 0 14 0.89543 14 2V3H18V5H16V18C16 19.1046 15.1046 20 14 20ZM4 5V18H14V5H4ZM6 2V3H12V2H6ZM12 16H10V7H12V16ZM8 16H6V7H8V16Z" fill="#2E3A59"/>
                    </svg>
                    <svg className="ms-3"xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                    <path d="M1.13107 13.5767C0.932373 13.5764 0.742955 13.4926 0.609031 13.3458C0.472638 13.2002 0.404864 13.0033 0.422739 12.8046L0.596281 10.8964L8.6132 2.8823L11.1186 5.38696L3.10378 13.4003L1.19553 13.5739C1.17357 13.576 1.15161 13.5767 1.13107 13.5767ZM11.6187 4.88617L9.11399 2.38151L10.6164 0.879131C10.7492 0.746123 10.9295 0.671387 11.1175 0.671387C11.3055 0.671387 11.4858 0.746123 11.6187 0.879131L13.121 2.38151C13.254 2.51437 13.3288 2.69465 13.3288 2.88265C13.3288 3.07065 13.254 3.25094 13.121 3.3838L11.6194 4.88546L11.6187 4.88617Z" fill="#2E3A59"/>
                    </svg>
                  </td>
                </tr>
              </tbody>
              <br></br>
            </table>
            <div>
            <nav aria-label="Page navigation example">
              <ul className="pagination text-end d-flex justify-content-end">
                <li class="page-item"><a class="page-link text-dark" href="#">Previous</a></li>
                <li class="page-item"><a class="page-link text-dark" href="#">1</a></li>
                <li class="page-item"><a class="page-link text-dark" href="#">2</a></li>
                <li class="page-item"><a class="page-link text-dark" href="#">3</a></li>
                <li class="page-item"><a class="page-link text-dark" href="#">Next</a></li>
              </ul>
            </nav>
          </div>
          </div>      
          </div>
            
            
            
            </div>
        
      </div>
    </div>
    
  )
}

export default ViewStudentLog;