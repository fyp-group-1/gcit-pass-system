<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('student_id')->unique();
            $table->string('name');
            $table->string('semester');
            $table->string('gender');
            $table->string('dzongkhag')->nullable();
            $table->string('cid')->nullable()->unique();
            $table->string('dob')->nullable(); 
            $table->string('phone_no')->nullable();
            $table->string('programme')->nullable();
            $table->string('admission_year')->nullable(); 
            $table->string('academic_year')->nullable();
            $table->string('email')->unique(); 
            $table->string('password')->nullable();
            $table->unsignedBigInteger('role_id')->unique(); 
            $table->string('role')->nullable(); 
            $table->json('access')->nullable(); 
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('students');
    }
};
