<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('staffs', function (Blueprint $table) {
            $table->id();
            $table->string('staff_id')->unique();
            $table->string('name');
            $table->string('gender');
            $table->string('dob')->nullable();
            $table->string('date_of_joining')->nullable();
            $table->string('country')->nullable();
            $table->string('cid')->unique();
            $table->string('phone_no')->nullable();
            $table->string('email')->nullable();
            $table->string('major_group')->nullable();
            $table->string('position_level')->nullable();
            $table->string('position_title')->nullable();
            $table->string('employee_type')->nullable();
            $table->string('department/division')->nullable();
            $table->string('department/section')->nullable();
            $table->string('password')->nullable();
            $table->string('role_id')->nullable();
            $table->string('role')->nullable();
            $table->json('access')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('staffs');
    }
};
