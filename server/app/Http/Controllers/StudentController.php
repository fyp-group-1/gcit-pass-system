<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StudentController extends Controller
{
     // Display a listing of the students
     public function index()
     {
         $students = Student::all();
         return view('students.index', ['students' => $students]);
     }
 
     // Show the form for creating a new student
     public function create()
     {
         return view('students.create');
     }
 
     // Store a newly created student in the database
     public function store(Request $request)
     {
         // Validate and save the student data
         // You can use $request->input('field_name') to access form input data
         // Example: $student = new Student();
         // $student->name = $request->input('name');
         // $student->save();
 
         // Redirect to the student list or show page
     }
 
     // Display the specified student
     public function show($id)
     {
         $student = Student::find($id);
         return view('students.show', ['student' => $student]);
     }
 
     // Show the form for editing a student
     public function edit($id)
     {
         $student = Student::find($id);
         return view('students.edit', ['student' => $student]);
     }
 
     // Update the specified student in the database
     public function update(Request $request, $id)
     {
         // Validate and update the student data
         // Redirect to the student list or show page
     }
 
     // Remove the specified student from the database
     public function destroy($id)
     {
         $student = Student::find($id);
         $student->delete();
 
         // Redirect to the student list
     }
     public function rolename(Request $request)
     {
         // Assuming you're passing the student_id as a parameter in the request
         $studentId = $request->input('student_id');
 
         // Fetch the student record from the database based on the student_id
         $student = Student::where('student_id', $studentId)->first();
 
         if ($student) {
             $role = $student->role; // Assuming 'role' is the name of the column that stores the role
 
             return response()->json(['role' => $role], 200);
         } else {
             // Handle the case where the student is not found
             return response()->json(['message' => 'Student not found'], 404);
         }
     }

}
