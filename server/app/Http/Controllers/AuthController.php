<?php


namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Student;
use App\Models\Staff;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    // public function register(Request $request)
    // {
    //     $request->validate([
    //         'name' => 'required|max:255',
    //         'email' => 'required|email|unique:users',
    //         'password' => 'required|min:8',
    //     ]);

    //     $user = User::create([
    //         'name' => $request->name,
    //         'email' => $request->email,
    //         'password' => Hash::make($request->password),
    //     ]);

    //     $token = auth()->login($user);

    //     return $this->respondWithToken($token);
    // }


    public function login(Request $request)
        {
            $credentials = $request->only('email', 'password');
            $email = $credentials['email']; // Retrieve the email from the credentials array
            $password = $credentials['password'];
            
            $student = Student::where('email', $email)->first(); // Use Eloquent to retrieve the student
            $staff = Staff::where('email', $email)->first();
            if ($student || $staff) {
                if($student){
                    $storedPassword = $student->password; 
                    if (Hash::check($password, $storedPassword)) {
    
                        $token = $student->createToken('GCIT_GATE_PASS_TOKEN')->accessToken;
                        
                        return $this->respondWithToken($token, $student->id, $student->role);
                    } else {
                        return response()->json(['error' => 'Incorrect password'], 401);
                    }
                }
                if($staff){
                    $storedPassword = $staff->password; 
                    if (Hash::check($password, $storedPassword)) {
    
                        $token = $staff->createToken('GCIT_GATE_PASS_TOKEN')->accessToken;
                        
                        return $this->respondWithToken($token, $staff->id, $staff->role);
                    } else {
                        return response()->json(['error' => 'Incorrect password'], 401);
                    }
                }
            } else {
                return response()->json(['error' => 'User not found'], 404);
            }
        }



    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Logged out successfully']);
    }

    public function sendResetPasswordEmail(Request $request)
    {
        $request->validate(['email' => 'required|email']);

        $status = Password::sendResetLink(
            $request->only('email'),
            function (Message $message) {
                $message->subject('Reset Your Password');
            }
        );

        return $status === Password::RESET_LINK_SENT
            ? response()->json(['message' => 'Password reset link sent to your email'], Response::HTTP_OK)
            : response()->json(['message' => 'Unable to send password reset link'], Response::HTTP_BAD_REQUEST);
    }

    protected function respondWithToken($token, $id, $role)
    {
        $uniqueToken = Str::random(92); 
        return response()->json([
            'access_token' => $token,
            'user_id' => $id,
            'role'=> $role,

            // 'expires_in' => auth('api')->factory()->getTTL() * 60,
        ]);
    }
    
}









